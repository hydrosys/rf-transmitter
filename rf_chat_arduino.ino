#include <SPI.h>
#include <avr/sleep.h>
#include "nRF24L01.h"
#include "RF24.h"
//#include "printf.h"

RF24 radio(40, 53);
//RF24 radio(9,10);

const uint64_t pipes[2] = { 0xDEDEDEDEE7LL, 0xDEDEDEDEE9LL };

boolean stringComplete = false;  // whether the string is complete
static int dataBufferIndex = 0;
boolean stringOverflow = false;
char charOverflow = 0;

char SendPayload[31] = "";
char RecvPayload[31] = "";
char serialBuffer[31] = "";

typedef enum {enter_standby, send_data, start, teste} states;
//states STATE = start;
states STATE = enter_standby;

// Sleep declarations
typedef enum { wdt_16ms = 0, wdt_32ms, wdt_64ms, wdt_128ms, wdt_250ms, wdt_500ms, wdt_1s, wdt_2s, wdt_4s, wdt_8s } wdt_prescalar_e;

void setup_watchdog(uint8_t prescalar);
void do_sleep(void);

const short sleep_cycles_per_transmission = 4;
volatile short sleep_cycles_remaining = sleep_cycles_per_transmission;

int wakePin = 22;                 // pin used for waking up
int sleepStatus = 0;             // variable to store a request for sleep
int count = 0;                   // counter

void wakeUpNow()        // here the interrupt is handled after wakeup
{
  // execute code here after wake-up before returning to the loop() function
  // timers and code using timers (serial.print and more...) will not work here.
  // we don't really need to execute any special functions here, since we
  // just want the thing to wake up
  Serial.println("Wake up now");
  nRF_receive();
  Serial.println(STATE);
  if (STATE == enter_standby && (strcmp(RecvPayload, "wup") == 0)) {
    Serial.println("STATE = start");
    STATE = start;
    RecvPayload[0];
  }
}

void setup(void) {

  Serial.begin(115200);

  Serial.println("RF Chat V0.90");

  // Prepare sleep parameters
  // Only the ping out role uses WDT.  Wake up every 4s to send a ping
  //if ( role == role_ping_out )
  //setup_watchdog(wdt_4s);
  pinMode(wakePin,INPUT);
  digitalWrite(wakePin,HIGH);

  /* Now it is time to enable an interrupt. In the function call
   * attachInterrupt(A, B, C)
   * A   can be either 0 or 1 for interrupts on pin 2 or 3.   
   *
   * B   Name of a function you want to execute while in interrupt A.
   *
   * C   Trigger mode of the interrupt pin. can be:
   *             LOW        a low level trigger
   *             CHANGE     a change in level trigger
   *             RISING     a rising edge of a level trigger
   *             FALLING    a falling edge of a level trigger
   *
   * In all but the IDLE sleep modes only LOW can be used.
   */

  attachInterrupt(0, wakeUpNow, LOW); // use interrupt 0 (pin 2) and run function
                                      // wakeUpNow when pin 2 gets LOW
    
  //printf_begin();
  radio.begin();

  radio.setDataRate(RF24_250KBPS);
  radio.setPALevel(RF24_PA_MAX);
  radio.setChannel(70);

  radio.enableDynamicPayloads();
  radio.setRetries(15, 15);
  radio.setCRCLength(RF24_CRC_16);

  radio.openWritingPipe(pipes[0]);
  radio.openReadingPipe(1, pipes[1]);

  //radio.powerUp();
  //delay(10);
  
  radio.startListening();
  radio.printDetails();

  Serial.println();
  Serial.println("RF Chat V0.90");
  delay(500);

}


void sleepNow()         // here we put the arduino to sleep
{
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);   // sleep mode is set here

    sleep_enable();          // enables the sleep bit in the mcucr register
                             // so sleep is possible. just a safety pin

    attachInterrupt(0, wakeUpNow, LOW); // use interrupt 0 (pin 2) and run function
                                       // wakeUpNow when pin 2 gets LOW

    sleep_mode();            // here the device is actually put to sleep!!
                             // THE PROGRAM CONTINUES FROM HERE AFTER WAKING UP

    sleep_disable();         // first thing after waking from sleep:
                             // disable sleep...
    detachInterrupt(0);      // disables interrupt 0 on pin 2 so the
                             // wakeUpNow code will not be executed
                             // during normal running time.

}

void loop(void)
{
  switch (STATE)
  {
    case start:
      if (test_communication())
      {
        STATE = send_data;
      }
      break;
    case send_data:
      break;
    case enter_standby:
      Serial.println("Sleep now");
      //radio.powerDown(); //com esse cmd so printa RF
      //para economizar bateria existe a opção de radio.setautoAck(false)
      delay(100);
      sleepNow();
      break;

    case teste:
      //nRF_receive();  
      break;
  }
} // end loop()

bool test_communication() 
{
  bool result = false;

  nRF_receive();
  if (strcmp(RecvPayload, "snd") ==0) {
    delay(1000); //espera 1 segundo para enviar o rcv
    serial_send("rcv");
    result = true;
    RecvPayload[0] = 0;  // Clear the buffers
  }

  return result;
}

void nRF_receive(void) 
{
  int len = 0;
  if ( radio.available() ) {
    bool done = false;
    while ( radio.available() ) {
      len = radio.getDynamicPayloadSize();
      radio.read(&RecvPayload, len);
      delay(5);
    }

    RecvPayload[len] = 0; // null terminate string

    Serial.print("R:");
    Serial.print(RecvPayload);
    Serial.println();
    //RecvPayload[0] = 0;  // Clear the buffers
  }

} // end nRF_receive()

void serial_send(char *str)
{
 
  strcat(SendPayload,str);  
  // swap TX & 0 addr for writing
  radio.openWritingPipe(pipes[1]);
  radio.openReadingPipe(0,pipes[0]);
  radio.stopListening();
  bool ok = radio.write(&SendPayload,strlen(SendPayload));

  Serial.print("S:");
  Serial.print(SendPayload);      
  Serial.println();
  stringComplete = false;

  // restore TX & Rx addr for reading    
  radio.openWritingPipe(pipes[0]);
  radio.openReadingPipe(1,pipes[1]);
  radio.startListening();
  SendPayload[0] = 0;
  dataBufferIndex = 0;
}   

