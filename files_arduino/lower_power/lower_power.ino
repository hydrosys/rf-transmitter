#include <avr/sleep.h>
#include <avr/power.h>
#include <DS3232RTC.h>
#include <Wire.h>



int pin2 = 2;

void pin2Interrupt(void) 
{

  detachInterrupt(0);
  Serial.println("wake up");
}

void enterSleep(void) {
  EIFR = bit (0);
  /* Setup pin2 as an interrupt and attach handler. */
  attachInterrupt(0, pin2Interrupt,FALLING);
  delay(100);
  
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
 
  sleep_enable();
  //Serial.println("Sleeping...");
  
  sleep_mode();
 
  /* The program will continue from here. */
 Serial.println("Continuando daqui");
  /* First thing to do is disable sleep. */
  sleep_disable();
}

void setup() {
  Serial.begin(115200);
 
  /* Setup the pin direction. */
  pinMode(pin2, INPUT);
  digitalWrite(pin2, HIGH);

  setTime(23,31,30,13,2,2009);
  RTC.set(now());
  //Serial.println(RTC.set(now()));
  RTC.squareWave(SQWAVE_NONE);
  //RTC.setAlarm(ALM1_EVERY_SECOND, 1, 0, 0,0);
  RTC.setAlarm(ALM1_MATCH_SECONDS, 1, 0, 0,0);
  RTC.alarmInterrupt(ALARM_1, true);
 
 
  Serial.println("Initialisation complete.");
}

int seconds = 0;

void loop() {
  delay(1000);
  seconds++;

  Serial.print("Awake for ");
  Serial.print(seconds, DEC);
  Serial.println(" second");
  //digitalClockDisplay(); //print time and date

 // Serial.println("Entering sleep");
    //digitalClockDisplay();
  //  delay(200);
   // seconds = 0;
   // enterSleep();
    

  if(RTC.alarm(ALARM_1))
   {
     Serial.println("Disparou!!!");}
   
 if(seconds == 3) {
    Serial.println("Entering sleep");
    //digitalClockDisplay();
    delay(200);
    seconds = 0;
    enterSleep();
    
  }
 
}


