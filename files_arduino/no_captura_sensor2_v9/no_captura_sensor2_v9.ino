#include <SPI.h>
#include <avr/sleep.h>
#include "nRF24L01.h"
#include "RF24.h"
#include <string.h>
#include <avr/power.h>
#include <DS3232RTC.h>
#include <Wire.h>
#include <TimeLib.h>

//RF24 radio(40, 53);//MEGA
RF24 radio(9, 10);

const uint64_t pipes[3] = { 0xA0A0A0A03F, 0xF0F0F0F0AA, 0xF0F0F0F066 };

boolean stringComplete = false;  // whether the string is complete
static int dataBufferIndex = 0;
boolean stringOverflow = false;
char charOverflow = 0;

char SendPayload[31] = "";
char RecvPayload[31] = "";
char serialBuffer[31] = "";

typedef enum {enter_standby, send_data, start, teste, ext} states;
states STATE = send_data;

typedef enum {setup_send_data, wait_rcv, send_flow} states_send_data;
states_send_data state_send_data = setup_send_data;


int wakePin = 3;                 // pin used for waking up
int count = 0;                   // counter

unsigned long new_time;
unsigned long old_time = 0;
unsigned long TIMEOUT = 300;
unsigned long t = 5000;

char strA1[] = "a2";
char temp[40] = "";

//flow
unsigned long flowRate;
// The hall-effect flow sensor outputs approximately 4.5 pulses per second per
// litre/minute of flow.
float calibrationFactor = 4.5;
volatile byte pulseCount = 0;
float f_flowRate = 0.0;
unsigned int flowMilliLitres = 0;
unsigned long totalMilliLitres = 0;
unsigned long oldTime;
char s_flowRate[50];
bool setup_system = true;


volatile int NbTopsFan; //measuring the rising edges of the signal
int Calc;
int hallsensor = 2;    //The pin location of the sensor
//-----------------------------------------------------------------------------------
void setup(void) {

  Serial.begin(115200);

  Serial.println("No Captura- sensor 1");

  pinMode(wakePin, INPUT);
  digitalWrite(wakePin, HIGH);
  setTime(23,31,30,13,2,2009);
  RTC.set(now());
  RTC.squareWave(SQWAVE_NONE);
  //RTC.setAlarm(ALM1_EVERY_SECOND, 1, 0, 0,0);
  RTC.setAlarm(ALM1_MATCH_SECONDS, 1, 0, 0,0);
  RTC.alarmInterrupt(ALARM_1, true);
  detachInterrupt(1);
  
  radio.begin();

  radio.setDataRate(RF24_250KBPS);
  radio.setPALevel(RF24_PA_MAX);
  radio.setChannel(70);

  radio.enableDynamicPayloads();
  radio.setRetries(15, 15);
  radio.setCRCLength(RF24_CRC_16);

  radio.setAutoAck(true);

  radio.openWritingPipe(pipes[1]);
  radio.openReadingPipe(1, pipes[0]);


  radio.startListening();

  pinMode(hallsensor, INPUT); //initializes digital pin 2/3 as an input
  attachInterrupt(digitalPinToInterrupt(hallsensor), rpm, RISING); //and the interrupt is attached

  delay(500);


}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------

void loop(void)
{

  nRF_receive();
  switch (STATE)
  {

    /*case start:
      if (test_communication())
      {
        STATE = send_data;
        state_send_data = setup_send_data;

      }
      break;*/

    case send_data:

      flowRate = pega_fluxo();

      switch (state_send_data)
      {
        case setup_send_data:

          if (strcmp(RecvPayload, "inf") == 0)  //se o arduino receber requisicao para enviar os dados, envia
          {
            sprintf(temp, "%lu", flowRate);
            strcat(s_flowRate, strA1);
            strcat(s_flowRate, temp);
            Serial.println("REQUISICAO CHEGOU");
            //delay(1000);
           
            state_send_data = send_flow;
            old_time = millis();
            setup_system = false;

          }
          if (strcmp(RecvPayload, "s") == 0)
          { 
            Serial.println("standby");
             //enterSleep();
             
          }
          //Provavelmente amanheceu
          if (!setup_system)
          {
            if (strcmp(RecvPayload, "snd") == 0)
            {
              Serial.println("ANOITECEU");
              STATE = start;
              setup_system = true;
            }
          }
          break;


        case send_flow:

          Serial.println("send flow");
          delay(500);
          
          serial_send(s_flowRate); //MANDAR FLUXO
          

          /*if (strcmp(RecvPayload, "rcvf1") == 0)
          {
            Serial.println("RECEBEU");
            STATE = send_data;
            state_send_data = setup_send_data;
            old_time = millis();
            STATE = enter_standby;
          }*/
          
          //STATE = enter_standby;
          state_send_data = setup_send_data;
          RecvPayload[0]=0;
          strcpy(s_flowRate, "");
          strcpy(temp, "");
          break;
      }
      break;
    case ext:
      Serial.println("FINISH");
      break;
    case enter_standby:
      Serial.println("Sleep now");

      //para economizar bateria existe a opção de radio.setautoAck(false)
      delay(100);

      break;

    case teste:
      //nRF_receive();
      STATE = send_data;
      break;
  }
} // end loop()

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

void pinInterrupt(void) 
{

  detachInterrupt(1);
  Serial.println("wake up");
}
//--------------------------------------------------------------------------------------
void enterSleep(void) {
  EIFR = bit (0);
  /* Setup pin2 as an interrupt and attach handler. */
  attachInterrupt(1, pinInterrupt,FALLING);
  delay(100);
  
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
 
  sleep_enable();
  Serial.println("Sleeping...");
  
  sleep_mode();
 
  /* The program will continue from here. */
 Serial.println("Continuando daqui");
  /* First thing to do is disable sleep. */
  sleep_disable();
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void rpm ()     //This is the function that the interupt calls
{
  NbTopsFan++;  //This function measures the rising and falling edge of the

}


//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


unsigned long pega_fluxo()
{
  NbTopsFan = 0;      //Set NbTops to 0 ready for calculations
  sei();            //Enables interrupts
  //attachInterrupt(digitalPinToInterrupt(hallsensor), rpm, RISING); //and the interrupt is attached
  
  delay(1000);      //Wait 1 second
  cli();            //Disable interrupts
  //detachInterrupt(digitalPinToInterrupt(hallsensor));
  Calc = (NbTopsFan * 60 / 7.5); //(Pulse frequency x 60) / 7.5Q, = flow rate in L / hour
  Serial.println (Calc, DEC); //Prints the number calculated above
  //Serial.print (" L/hour\r\n"); //Prints "L/hour" and returns a  new line

  return Calc;
}
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

bool test_communication()
{
  bool result = false;

  //nRF_receive();
  if (strcmp(RecvPayload, "snd") == 0)
  {
    delay(1000); //espera 1 segundo para enviar o rcv
    serial_send("rcv");
    result = true;
    RecvPayload[0] = 0;  // Clear the buffers
  }

  return result;
}
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
bool timeout(unsigned long t)
{
  new_time = millis();
  if (new_time - old_time > t)
  {
    old_time = new_time;
    return true;
  }
  else
  {
    return false;
  }

}
//---------------------------------------------------------------------------------------------------------------------------------------------------
void nRF_receive(void)
{

  int len = 0;
  if ( radio.available() ) {
    bool done = false;
    while ( radio.available() ) {
      len = radio.getDynamicPayloadSize();
      radio.read(&RecvPayload, len);
      delay(5);
    }

    RecvPayload[len] = 0; // null terminate string

    Serial.print("R:");
    Serial.print(RecvPayload);
    Serial.println();
    //RecvPayload[0] = 0;  // Clear the buffers
  }
  else
  {
    delay(5);
  }

} // end nRF_receive()
//-------------------------------------------------------------------------------------------------------------------------------------------------------
void serial_send(char *str)
{

  strcat(SendPayload, str);

  radio.stopListening();

  //Serial.print(SendPayload);
  bool ok = radio.write(&SendPayload, strlen(SendPayload));


  //Serial.println(ok);
  Serial.print("S:");
  Serial.print(SendPayload);
  Serial.println();
  stringComplete = false;

  radio.startListening();
  SendPayload[0] = 0;
  dataBufferIndex = 0;
}
