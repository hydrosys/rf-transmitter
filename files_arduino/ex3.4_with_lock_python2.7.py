import socket
from threading import Thread
import threading
import time
import inspect

#lock
lock = threading.Lock()

#funcao que printa com lock
def printf(string):
	#print "Acquiring lock"
	lock.acquire()
	try:
		#print "Lock Acquired"
		print string
	finally:
		lock.release()
		#print "Lock Released"

#define
N = 4 #tamanho do buffer
TIMEOUT = 2.5 #tempo limite
TESTE = True #para testar perda de pacotes
LAST_PACKET = 15 #ultimo pacote a ser enviado
REFUSE_DATA = True #flag para evitar varios printe de refuse_data

printf("3-UDP + Protocolo de Aplicacao (GO-BACK-N)")

#ip e porta do sender
UDP_IP_SENDER = "127.0.0.2"
UDP_PORT_SENDER = 5005

#ip e porta do receiver
UDP_IP_RECEIVER = "127.0.0.1"
UDP_PORT_RECEIVER = 5005

#variaveis iniciais do sender
base = 0
nextseqnum = 0
start_timer = time.time() #tempo atual
stop_timer = False
sndpkt = []

#cria sockets para o sender e o sender_receiver
sock_sender = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock_sender_receiver = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock_sender_receiver.bind((UDP_IP_SENDER, UDP_PORT_SENDER))

#cria sockets para o receiver_sender e o receiver
sock_receiver_sender = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock_receiver = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock_receiver.bind((UDP_IP_RECEIVER, UDP_PORT_RECEIVER))

#obtem o numero do ACK
def getacknum(rvcpkt):
	#extrai data
	data = rdt_rcv(rvcpkt)
	#obtem o indice do "#" em data
	i = data.rfind("#")
	#obtem o numero a partir do indice
	num = data[i+1:]
	#retorna o num em inteiro
	return int(num)

def refuse_data(data):
	printf("(sender) refuse data: can't send pkt because buffer is full")

#verifica se ocorreu timeout
def timeout():
	global start_timer
	#se nao foi pedido para parar o timer
	if stop_timer == False:
		if time.time() - start_timer > TIMEOUT:
			return True
		else:
			return False
	else:
		return False

#socket de sender envia packet para receiver
def udt_send(packet):
	sock_sender.sendto(packet, (UDP_IP_RECEIVER, UDP_PORT_RECEIVER))

#cria pacote
def make_pkt(nextseqnum, data):
	#coloca #[numero do pacote] no final da string data e codifica
	return (data + "#" + str(nextseqnum)).encode()

def rdt_send(data):
	global nextseqnum
	global sndpkt
	global TESTE
	global REFUSE_DATA
	#se o numero de pacotes a ser enviados estiver dentro do tamanho do buffer
	if nextseqnum < base + N:
		#cria lista de pacotes
		sndpkt.append(make_pkt(nextseqnum, data))
		#testa perda de pacotes do sender
		if TESTE:
			#pacotes 10 em diante serao perdidos
			if nextseqnum < 10:
				udt_send(sndpkt[nextseqnum])
		else:
			#envia pacote para o receiver
			udt_send(sndpkt[nextseqnum])
		printf("(sender) send " + sndpkt[nextseqnum].decode())
		#se buffer estiver vazio
		if base == nextseqnum:
			#inicia timer
			start_timer = time.time()
			stop_timer = False
			printf("(sender) start timer")
		#proximo numero de pacote
		nextseqnum += 1
		printf("(sender) next sequence number: " + str(nextseqnum))
		REFUSE_DATA = True
	else:
		#nao envia pacote
		if REFUSE_DATA:
			refuse_data(data)
			REFUSE_DATA = False

def sender():
	global N
	global start_timer
	global stop_timer
	global nextseqnum
	while True:
		data = "pkt"
		#programa termina antes do pacote 30 ser enviado
		if nextseqnum == LAST_PACKET + 1:
			#envia comando sair para receiver
			udt_send("exit")
			#termina thread_sender 
			exit()
		#verifica se ocorreu timeout
		if timeout() == False:
			rdt_send(data)
		elif timeout() == True:
			printf("(sender) timeout")
			start_timer = time.time()
			printf("(sender) start timer")
			#envia todos os pacotes nao recebido pelo receiver
			for x in range(base, nextseqnum):
				udt_send(sndpkt[x])
				printf("(sender) re-send pkt" + str(x) )
			
def sender_receiver():
	global start_timer
	global stop_timer
	global base
	while True:
		#recebe ACK do receiver
		rcvpkt = sock_sender_receiver.recvfrom(1024)
		#extrai data
		data = rdt_rcv(rcvpkt)
		if data == "exit":
			exit()
		printf("(sender_receiver) received message: " + data)
		#se data foi recebida e nao esta corrompida
		if data != "" and notcorrupt(rcvpkt):
			#quando recebe o ACK a base eh modifica para garantir envio e recebimento de novos pacotes
			base = getacknum(rcvpkt) + 1
			printf("(sender_receiver) base: " + str(base))
			#se todos os pacotes enviados foram recebidos
			if base == nextseqnum:
				#parar timer
				stop_timer = True
				printf("(sender_receiver) stop timer")
			else:
				start_timer = time.time()
				stop_timer = False
				printf("(sender_receiver) start timer")
		elif data != "" and corrupt(rcvpkt):
			printf("(sender_receiver) data is corrupted")

#verifica se rvcpkt eh corrupt
def corrupt(rvcpkt):
	if rvcpkt == "":
		return True
	else:
		return False

#verifica se rvcpkt nao eh corrupt
def notcorrupt(rvcpkt):
	return not corrupt(rvcpkt)

#verifica se o pacote eh o esperado
def hasseqnum(rvcpkt, expectedseqnum):
	#extrai data
	data = rdt_rcv(rvcpkt)
	#obtem o indice do "#" em data
	i = data.rfind("#")
	#obtem o numero a partir do indice
	num = data[i+1:]
	printf("(receiver) " + data + " has sequence number: " + str(num))
	#se num for igual ao esperado
	if num == str(expectedseqnum):
		return True
	else:
		return False

def deliver_data(data):
	return data.decode()

def extract(packet):
	data, addr = packet
	return data

def rdt_rcv(packet):
	data = extract(packet)
	return deliver_data(data)

def receiver():
	data = ""
	expectedseqnum = 0 #numero de sequencia esperado
	sndpkt = make_pkt(expectedseqnum, "ack") #pacote de ack padrao
	global TESTE
	while True:
		#recebe pacote
		rvcpkt = sock_receiver.recvfrom(1024)
		#extrai data
		data = rdt_rcv(rvcpkt)
		#se data for comando de sair
		if data == "exit":
			#envia comando de sair para sender_receiver
			sock_receiver_sender.sendto("exit".encode(), (UDP_IP_SENDER, UDP_PORT_SENDER))
			exit()
		#se data for recebida e nao estiver corrompida e tiver numero de sequencia esperado
		printf("(receiver) expected sequence number: " + str(expectedseqnum))
		printf("(receiver) received message: " + data)
		if data != "" and notcorrupt(rvcpkt) and hasseqnum(rvcpkt, expectedseqnum):
			#cria pacote ack
			sndpkt = make_pkt(expectedseqnum, "ack")
			#testa perda de pacote ack
			if TESTE:
				#no expectedseqnum 5 e 6, acks serao perdidos
				if expectedseqnum < 5 or expectedseqnum > 6:
					sock_receiver_sender.sendto(sndpkt, (UDP_IP_SENDER, UDP_PORT_SENDER))
			else:
				#envia pacote ack
				sock_receiver_sender.sendto(sndpkt, (UDP_IP_SENDER, UDP_PORT_SENDER))
			printf("(receiver) send " + sndpkt.decode())
			#proxima sequencia sera esperada
			expectedseqnum += 1
		else:
			#envia pacote padrao
			sock_receiver_sender.sendto(sndpkt, (UDP_IP_SENDER, UDP_PORT_SENDER))
			printf("(receiver) re-send " + sndpkt.decode())

#cria as threads
th_sender = Thread(target = sender)
th_receiver = Thread(target = receiver)
th_sender_receiver = Thread(target = sender_receiver)

#inicializa as threads
th_sender.start()
th_receiver.start()
th_sender_receiver.start()
