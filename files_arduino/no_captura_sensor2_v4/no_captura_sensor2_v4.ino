#include <SPI.h>
#include <avr/sleep.h>
#include "nRF24L01.h"
#include "RF24.h"


//RF24 radio(40, 53);
RF24 radio(9, 10); //UNO

const uint64_t pipes[3] = { 0xA0A0A0A03L, 0xF0F0F0F0AA, 0xF0F0F0F066 };

boolean stringComplete = false;  // whether the string is complete
static int dataBufferIndex = 0;
boolean stringOverflow = false;
char charOverflow = 0;

char SendPayload[31] = "";
char RecvPayload[31] = "";
char serialBuffer[31] = "";

typedef enum {enter_standby, send_data, start, teste, ext} states;
//states STATE = teste;
//states STATE = enter_standby;
states STATE = start;

typedef enum {setup_send_data, wait_rcv, send_flow} states_send_data;
states_send_data state_send_data = setup_send_data;

// Sleep declarations
typedef enum { wdt_16ms = 0, wdt_32ms, wdt_64ms, wdt_128ms, wdt_250ms, wdt_500ms, wdt_1s, wdt_2s, wdt_4s, wdt_8s } wdt_prescalar_e;

void setup_watchdog(uint8_t prescalar);
void do_sleep(void);

const short sleep_cycles_per_transmission = 4;
volatile short sleep_cycles_remaining = sleep_cycles_per_transmission;

int wakePin = 22;                 // pin used for waking up
int sleepStatus = 0;             // variable to store a request for sleep
int count = 0;                   // counter

unsigned long new_time;
unsigned long old_time = 0;
unsigned long TIMEOUT = 300;

//Variables for FSR
int fsrPin = A1;     // the FSR and 10K pulldown are connected to A1
int fsrReading;     // the analog reading from the FSR resistor divider
unsigned long fsrVoltage;     // the analog reading converted to voltage
unsigned long variacao = 0; //to measure the variation between the current value of pression and the previous value
unsigned long fsrVolt_prev = 0; //previous value

char temp[40] = "";

//flow
unsigned long flowRate;
// The hall-effect flow sensor outputs approximately 4.5 pulses per second per
// litre/minute of flow.
float calibrationFactor = 4.5;
volatile byte pulseCount = 0;
float f_flowRate = 0.0;
unsigned int flowMilliLitres = 0;
unsigned long totalMilliLitres = 0;
unsigned long oldTime;
char s_flowRate[50];
char strA2[] = "a2";
unsigned long t = 5000;
bool setup_system = true;

void setup(void) {

  Serial.begin(115200);

  Serial.println("No Captura - Sensor 2");
  delay(100);
  // Prepare sleep parameters
  // Only the ping out role uses WDT.  Wake up every 4s to send a ping
  //if ( role == role_ping_out )
  //setup_watchdog(wdt_4s);
  pinMode(wakePin, INPUT);
  digitalWrite(wakePin, HIGH);

  /* Now it is time to enable an interrupt. In the function call
     attachInterrupt(A, B, C)
     A   can be either 0 or 1 for interrupts on pin 2 or 3.

     B   Name of a function you want to execute while in interrupt A.

     C   Trigger mode of the interrupt pin. can be:
                 LOW        a low level trigger
                 CHANGE     a change in level trigger
                 RISING     a rising edge of a level trigger
                 FALLING    a falling edge of a level trigger

     In all but the IDLE sleep modes only LOW can be used.
  */
  //Pq vc ta aqui?
  //attachInterrupt(0, wakeUpNow, LOW); // use interrupt 0 (pin 2) and run function
  // wakeUpNow when pin 2 gets LOW

  //printf_begin();
  radio.begin();

  radio.setDataRate(RF24_250KBPS);
  radio.setPALevel(RF24_PA_MAX);
  radio.setChannel(70);

  radio.enableDynamicPayloads();
  radio.setRetries(15, 15);
  radio.setCRCLength(RF24_CRC_16);

  radio.setAutoAck(true);

  radio.openWritingPipe(pipes[2]);
  radio.openReadingPipe(1, pipes[0]);

  //radio.powerUp();
  //delay(10);

  radio.startListening();

  attachInterrupt(digitalPinToInterrupt(3) , pulseCounter, FALLING);


  delay(500);


}
//-----------------------------------------------------------------------------------------------------------------------------------------------------------

void loop(void)
{

  nRF_receive();
  switch (STATE)
  {
    case start:
      //Serial.println("state == start");
      if (test_communication())
      {
        STATE = send_data;
        state_send_data = setup_send_data;

      }
      break;

    case send_data:
      //Serial.println("state == send_data");
      //TEM QUE VER COMO VAI PEGAR OS DADOS NESSE CASO
      flowRate = pega_fluxo();

      switch (state_send_data)
      {
          Serial.println("state == state_send_data");
        case setup_send_data:

          if (strcmp(RecvPayload, "inf2") == 0)  //se o arduino receber requisicao para enviar os dados, envia
          {
            Serial.println("REQUISICAO CHEGOU");
            sprintf(temp, "%lu", flowRate);
            strcat(s_flowRate, strA2);
            strcat(s_flowRate, temp);
            //delay(5);
            state_send_data = send_flow;
            old_time = millis();
            setup_system = false;

          }
          if (!setup_system)
          {
            if (timeout(2 * t))
            {
              Serial.println("TIMEOUT");
              STATE = start;
              setup_system = true;
              
            }
          }


          break;

        case send_flow:

          Serial.println("send flow");
          serial_send(s_flowRate); //MANDAR FLUXO
          strcpy(s_flowRate, "");
          strcpy(temp, "");
          delay(105);
          state_send_data = wait_rcv;
          //STATE = ext;
          break;

        case wait_rcv:
          Serial.println("wait rcv f");
          //nRF_receive();
          if (strcmp(RecvPayload, "rcvf2") == 0)
          {

            STATE = send_data;
            state_send_data = setup_send_data;
            old_time = millis();
          }
          else
          {
            state_send_data = send_flow;
          }

          break;
      }
      break;
    case ext:
      Serial.println("FINISH");
      break;
    case enter_standby:
      Serial.println("Sleep now");
      //radio.powerDown(); //com esse cmd so printa RF
      //para economizar bateria existe a opção de radio.setautoAck(false)
      delay(100);
      sleepNow();
      break;

    case teste:
      //nRF_receive();
      STATE = send_data;
      break;
  }
} // end loop()

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
void wakeUpNow()        // here the interrupt is handled after wakeup
{
  // execute code here after wake-up before returning to the loop() function
  // timers and code using timers (serial.print and more...) will not work here.
  // we don't really need to execute any special functions here, since we
  // just want the thing to wake up
  Serial.println("Wake up now");
  //nRF_receive();
  Serial.println(STATE);
  if (STATE == enter_standby && (strcmp(RecvPayload, "wup") == 0)) {
    Serial.println("STATE = start");
    STATE = start;
    RecvPayload[0] = 0;
  }
}

//--------------------------------------------------------------------------------------------------------------------------------------------------------------------
void sleepNow()         // here we put the arduino to sleep
{
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);   // sleep mode is set here

  sleep_enable();          // enables the sleep bit in the mcucr register
  // so sleep is possible. just a safety pin

  attachInterrupt(0, wakeUpNow, LOW); // use interrupt 0 (pin 2) and run function
  // wakeUpNow when pin 2 gets LOW

  sleep_mode();            // here the device is actually put to sleep!!
  // THE PROGRAM CONTINUES FROM HERE AFTER WAKING UP

  sleep_disable();         // first thing after waking from sleep:
  // disable sleep...
  detachInterrupt(0);      // disables interrupt 0 on pin 2 so the
  // wakeUpNow code will not be executed
  // during normal running time.

}
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------
void pulseCounter()
{
  // Increment the pulse counter
  pulseCount++;
}


//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
unsigned long pega_fluxo()
{
  if ((millis() - oldTime) > 1000)   // Only process counters once per second
  {
    // Disable the interrupt while calculating flow rate and sending the value to
    // the host
    detachInterrupt(digitalPinToInterrupt(3));

    // Because this loop may not complete in exactly 1 second intervals we calculate
    // the number of milliseconds that have passed since the last execution and use
    // that to scale the output. We also apply the calibrationFactor to scale the output
    // based on the number of pulses per second per units of measure (litres/minute in
    // this case) coming from the sensor.
    f_flowRate = ((1000.0 / (millis() - oldTime)) * pulseCount) / calibrationFactor;

    // Note the time this processing pass was executed. Note that because we've
    // disabled interrupts the millis() function won't actually be incrementing right
    // at this point, but it will still return the value it was set to just before
    // interrupts went away.
    oldTime = millis();

    // Divide the flow rate in litres/minute by 60 to determine how many litres have
    // passed through the sensor in this 1 second interval, then multiply by 1000 to
    // convert to millilitres.
    flowMilliLitres = (f_flowRate / 60) * 1000;

    // Add the millilitres passed in this second to the cumulative total
    totalMilliLitres += flowMilliLitres;

    unsigned int frac;

    // Print the flow rate for this second in litres / minute
    /*Serial.print("Flow rate: ");
      Serial.print(int(flowRate));  // Print the integer part of the variable
      Serial.print(".");             // Print the decimal point
      // Determine the fractional part. The 10 multiplier gives us 1 decimal place.
      frac = (flowRate - int(flowRate)) * 10;
      Serial.print(frac, DEC) ;      // Print the fractional part of the variable
      Serial.print("L/min");*/
    // Print the number of litres flowed in this second
    Serial.print("  Current Liquid Flowing: ");             // Output separator
    Serial.print(flowMilliLitres);
    Serial.println(" mL/Sec");

    /*    // Print the cumulative total of litres flowed since starting
        Serial.print("  Output Liquid Quantity: ");             // Output separator
        Serial.print(totalMilliLitres);
        Serial.println("mL"); */

    // Reset the pulse counter so we can start incrementing again
    pulseCount = 0;

    // Enable the interrupt again now that we've finished sending output
    attachInterrupt(digitalPinToInterrupt(3), pulseCounter, FALLING);
  }


  return flowMilliLitres;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------
bool test_communication()
{
  bool result = false;

  //nRF_receive();
  //Serial.println(RecvPayload);
  if (strcmp(RecvPayload, "snd2") == 0) {

    delay(1000); //espera 1 segundo para enviar o rcv
    serial_send("rcv2");
    result = true;
    RecvPayload[0] = 0;  // Clear the buffers
  }

  return result;
}
//---------------------------------------------------------------------------------------------------------------------------------------------------------
bool timeout(unsigned long t)
{
  new_time = millis();
  if (new_time - old_time > t)
  {
    old_time = new_time;
    return true;
  }
  else
  {
    return false;
  }

}
//---------------------------------------------------------------------------------------------------------------------------------------------------
void nRF_receive(void)
{
  //Serial.print("R:");
  int len = 0;
  if ( radio.available() ) {
    bool done = false;
    while ( radio.available() ) {
      len = radio.getDynamicPayloadSize();
      radio.read(&RecvPayload, len);
      delay(5);
    }

    RecvPayload[len] = 0; // null terminate string

    Serial.print("R:");
    Serial.print(RecvPayload);
    Serial.println();
    //RecvPayload[0] = 0;  // Clear the buffers
  }
  else
  {
    delay(5);
  }

} // end nRF_receive()
//-------------------------------------------------------------------------------------------------------------------------------------------------------
void serial_send(char *str)
{

  strcat(SendPayload, str);

  radio.stopListening();
  bool ok = radio.write(&SendPayload, strlen(SendPayload));

  Serial.print("S:");
  Serial.print(SendPayload);
  Serial.println();
  stringComplete = false;

  radio.startListening();
  SendPayload[0] = 0;
  dataBufferIndex = 0;
}


