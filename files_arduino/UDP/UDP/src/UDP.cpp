//============================================================================
// Name        : UDP.cpp
// Author      : Rodolfo Andrade
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
#include <algorithm>
#include <stdexcept>
#include <bitset>
using namespace std;

std::string string_to_hex(const std::string& input)
{
    static const char* const lut = "0123456789ABCDEF";
    size_t len = input.length();

    std::string output;
    output.reserve(2 * len);
    for (size_t i = 0; i < len; ++i)
    {
        const unsigned char c = input[i];
        output.push_back(lut[c >> 4]);
        output.push_back(lut[c & 15]);
    }
    return output;
}

std::string hex_to_string(const std::string& input)
{
    static const char* const lut = "0123456789ABCDEF";
    size_t len = input.length();
    if (len & 1) throw std::invalid_argument("odd length");

    std::string output;
    output.reserve(len / 2);
    for (size_t i = 0; i < len; i += 2)
    {
        char a = input[i];
        const char* p = std::lower_bound(lut, lut + 16, a);
        if (*p != a) throw std::invalid_argument("not a hex digit");

        char b = input[i + 1];
        const char* q = std::lower_bound(lut, lut + 16, b);
        if (*q != b) throw std::invalid_argument("not a hex digit");

        output.push_back(((p - lut) << 4) | (q - lut));
    }
    return output;
}

int convertHextoInt(char c) {
	switch (c) {
		case 'A': return 10;
		case 'B': return 11;
		case 'C': return 12;
		case 'D': return 13;
		case 'E': return 14;
		case 'F': return 15;
		default: return c - '0';
	}
}

const char* hex_char_to_bin(char c)
{
    // TODO handle default / error
    switch(toupper(c))
    {
        case '0': return "0000";
        case '1': return "0001";
        case '2': return "0010";
        case '3': return "0011";
        case '4': return "0100";
        case '5': return "0101";
        case '6': return "0110";
        case '7': return "0111";
        case '8': return "1000";
        case '9': return "1001";
        case 'A': return "1010";
        case 'B': return "1011";
        case 'C': return "1100";
        case 'D': return "1101";
        case 'E': return "1110";
        case 'F': return "1111";
		default: return "-1";
    }
}

std::string hex_str_to_bin_str(const std::string& hex)
{
    // TODO use a loop from <algorithm> or smth
    std::string bin;
    for(unsigned i = 0; i != hex.length(); ++i)
       bin += hex_char_to_bin(hex[i]);
    return bin;
}

int main() {
	int tamHex = 16/4; //16bits/(4bits/Hex)=4Hex
	int len, div, rest, r, rNext=0;
	string msg = "~~~~";
	string out, result, result_bits;
	string lut = "0123456789ABCDEF";

	//se n�o for multiplo de 16bits adiciona 0 no come�o
	out = string_to_hex(msg);
	len = out.length();
	rest = len % tamHex;
	if (rest != 0) {
		cout<<len<<" is not multiple of "<<tamHex<<endl;
		cout<<"rest is "<<rest<<endl;
		out.insert(0, rest, '0');
		len = len + rest;
	}
	cout<<out<<endl;
	div = len / tamHex;
	//adi��o hexadecimal
	for (int i = tamHex-1; i >= 0; i--) {
		r = rNext;
		for (int j = 0; j < div; j++) {
			r = r + convertHextoInt(out[i+(j*tamHex)]);
		}
		rNext = 0;
		if (r >= 16) {
			r = r % 16;
			rNext = 1;
		}
		result.insert(0, 1, lut[r]);
	}
	//result.insert(0, 1, rNext + '0'); //n�o � poss�vel ter 10000
	cout<<result<<endl;
	result_bits = hex_str_to_bin_str(result);
	cout<<result_bits<<endl;
	bitset<16> foo(result_bits);
	cout<<foo.flip()<<endl;
	return 0;
}
