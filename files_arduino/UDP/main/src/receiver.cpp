//============================================================================
// Name        : receiver.cpp
// Author      : Rodolfo Andrade
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <string>
#include <stdio.h>

#include "udp.h"
using namespace std;

void sender_receiver() {
	string rcvpkt, data;
	bool exit = false;
	while (!exit) {
		//recebe ACK do receiver
		//rcvpkt = sock_sender_receiver.recvfrom(1024)
		//extrai data
		data = rdt_rcv(rcvpkt);
		if (data.compare("exit")) {
			exit = true;
		}
		printf("(sender_receiver) received message: %s", data.c_str());
		//se data foi recebida e nao esta corrompida
		if (!data.empty() && notcorrupt(rcvpkt)) {
			//quando recebe o ACK a base eh modifica para garantir envio e recebimento de novos pacotes
			base = getacknum(rcvpkt) + 1;
			printf("(sender_receiver) base: %d", base);
			//se todos os pacotes enviados foram recebidos
			if (base == nextseqnum) {
				//parar timer
				stop_timer = true;
				printf("(sender_receiver) stop timer");
			}
			else {
				//start_timer = time.time()
				start_timer = now();
				stop_timer = false;
				printf("(sender_receiver) start timer");
			}
		}
		else if (data != "" && corrupt(rcvpkt)) {
			printf("(sender_receiver) data is corrupted");
		}
	}
}

void receiver() {
	string data;
	int expectedseqnum = 0; //numero de sequencia esperado
	string sndpkt = make_pkt(expectedseqnum, "ack"), rvcpkt; //pacote de ack padrao
	bool exit = false;
	while (!exit) {
		//recebe pacote
		//rvcpkt = sock_receiver.recvfrom(1024)
		//extrai data
		data = rdt_rcv(rvcpkt);
		//se data for comando de sair
		if (data.compare("exit")) {
			//envia comando de sair para sender_receiver
			//sock_receiver_sender.sendto("exit".encode(), (UDP_IP_SENDER, UDP_PORT_SENDER))
			exit = true;
		}
		//se data for recebida e nao estiver corrompida e tiver numero de sequencia esperado
		printf("(receiver) expected sequence number: %d", expectedseqnum);
		printf("(receiver) received message: %s", data.c_str());
		if (!data.empty() && notcorrupt(rvcpkt) && hasseqnum(rvcpkt, expectedseqnum)) {
			//cria pacote ack
			sndpkt = make_pkt(expectedseqnum, "ack");
			//testa perda de pacote ack
			if (TEST) {
				//no expectedseqnum 5 e 6, acks serao perdidos
				if (expectedseqnum < 5 or expectedseqnum > 6) {
					//sock_receiver_sender.sendto(sndpkt, (UDP_IP_SENDER, UDP_PORT_SENDER))
				}
			}
			else {
				//envia pacote ack
				//sock_receiver_sender.sendto(sndpkt, (UDP_IP_SENDER, UDP_PORT_SENDER))
			}
			printf("(receiver) send %s", sndpkt.c_str());
			//proxima sequencia sera esperada
			expectedseqnum += 1;
		}
		else {
			//envia pacote padrao
			//sock_receiver_sender.sendto(sndpkt, (UDP_IP_SENDER, UDP_PORT_SENDER))
			printf("(receiver) re-send %s", sndpkt.c_str());
		}
	}
}
