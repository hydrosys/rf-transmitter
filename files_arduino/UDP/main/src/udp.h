/*
 * Checksum.h
 *
 *  Created on: 24 de jun de 2016
 *      Author: GAMER
 */

#ifndef UDP_H_
#define UDP_H_

#include <string>
#include <vector>

using namespace std;

static const int SIZE = 16;

//define
static const int N = 4; //tamanho do buffer
static const float TIMEOUT = 2.5; //tempo limite
static const bool TEST = true; //para testar perda de pacotes
static const int LAST_PACKET = 15; //ultimo pacote a ser enviado
static bool REFUSE_DATA = true; //flag para evitar varios printe de refuse_data

int now();

//variaveis iniciais do sender
static int base = 0;
static int nextseqnum = 0;
static time_t start_timer = now(); //tempo atual
static bool stop_timer = false;
static vector<string> sndpkt;

string string_to_binary(string input);

string fill_zeros(int SIZE, string input);

string add_binary(int SIZE, string input);

string inverse_binary(string input);

char binary_to_hex(string input);

string convert_binary_to_hex(string input);

string checksum(string data);

int getacknum(string rvcpkt);

void refuse_data(string data);

bool timeout();

void udt_send(string packet);

string make_pkt(int nextseqnum, string data);

void rdt_send(string data);

bool corrupt(string data);

bool notcorrupt(string data);

bool hasseqnum(string rvcpkt, int expectedseqnum);

string extract(string data);

string rdt_rcv(string packet);

#endif /* UDP_H_ */
