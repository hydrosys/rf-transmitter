//============================================================================
// Name        : sender.cpp
// Author      : Rodolfo Andrade
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <stdio.h>

#include "udp.h"
using namespace std;

void sender() {
	string data = "pkt";
	bool exit = false;
	while (!exit) {
		//programa termina depois do �ltimo pacote ser enviado
		if (nextseqnum == LAST_PACKET + 1) {
			//envia comando sair para receiver
			udt_send("exit");
			//termina thread_sender
			exit = true;
		}
		//verifica se ocorreu timeout
		if (timeout() == false) {
			rdt_send(data);
		}
		else if (timeout() == true) {
			printf("(sender) timeout");
			//start_timer = time.time()
			start_timer = now();
			printf("(sender) start timer");
			//envia todos os pacotes nao recebido pelo receiver
			for (int x = base; x < nextseqnum; x++) {
				udt_send(sndpkt[x]);
				printf("(sender) re-send pkt%d", x);
			}
		}
	}
}
