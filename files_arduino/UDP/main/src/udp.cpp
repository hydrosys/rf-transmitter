//============================================================================
// Name        : udp.cpp
// Author      : Rodolfo Andrade
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <stdio.h>
#include <string>
#include <bitset>
//#include <vector>
#include <cstdlib>
//#include <Time.h>

#include "udp.h"
using namespace std;

int now() {
	return 10;
}

string string_to_binary(string input) {
	string output, aux;
	for (int i = input.size() - 1; i >= 0; i--) {
		aux = bitset<8>(input.c_str()[i]).to_string();
		output.insert(0, aux);
	}
	//cout<<output<<endl;
	return output;
}

string fill_zeros(int SIZE, string input) {
	//se n�o for multiplo de 16bits adiciona 0s no come�o
	int len, rest;
	len = input.length();
	rest = len % SIZE;
	if (rest != 0) {
		cout<<len<<" is not multiple of "<<SIZE<<endl;
		cout<<"rest is "<<rest<<endl;
		input.insert(0, rest, '0');
	}
	return input;
}

string add_binary(int SIZE, string input) {
	int len, div, flag;
	string output, var;
	len = input.length();
	div = len/SIZE;
	//out.insert(0, SIZE, '0');
	output = input.substr(0,SIZE);
	cout<<"out: "<<output<<endl;
	for (int i=1;i<div;i++) {
		var = input.substr(i*SIZE,SIZE);
		cout<<"var: "<<var<<endl;
		for (int j=SIZE-1;j>=0;j--) {
			if (var[j]=='1' && output[j]=='1') {
				flag=1;
				for (int k=j;k>=0&&flag>0;k--) {
					if (output[k]=='1') {
						output[k]='0';
					}
					else {
						output[k]='1';
						flag--;
					}
				}
			}
			else if (var[j]=='1') {
				output[j]='1';
			}
		}
		cout<<"out: "<<output<<endl;
	}
	return output;
}

string inverse_binary(string input) {
	bitset<SIZE> foo(input);
	string aux = foo.flip().to_string();
	return aux;
}

char binary_to_hex(string input) {
	if (input.compare("0000")) return '0';
	if (input.compare("0001")) return '1';
	if (input.compare("0010")) return '2';
	if (input.compare("0011")) return '3';
	if (input.compare("0100")) return '4';
	if (input.compare("0101")) return '5';
	if (input.compare("0110")) return '6';
	if (input.compare("0111")) return '7';
	if (input.compare("1000")) return '8';
	if (input.compare("1001")) return '9';
	if (input.compare("1010")) return 'A';
	if (input.compare("1011")) return 'B';
	if (input.compare("1100")) return 'C';
	if (input.compare("1101")) return 'D';
	if (input.compare("1110")) return 'E';
	if (input.compare("1111")) return 'F';
	return '\0';
}

string convert_binary_to_hex(string input) {
	int len, div;
	char c;
	string output, var;
	len = input.length();
	div = len/(SIZE/4);
	//out.insert(0, SIZE, '0');
	cout<<"out: "<<output<<endl;
	for (int i=0;i<div;i++) {
		var = input.substr(i*SIZE/4,SIZE/4);
		cout<<"var: "<<var<<endl;
		c = binary_to_hex(var);
		output.insert(output.end(), c);
		cout<<"out: "<<output<<endl;
	}
	return output;
}

string checksum(string data) {
	string output;
	output = string_to_binary(data);
	output = fill_zeros(SIZE, output);
	cout<<output<<endl;
	output = add_binary(SIZE, output);
	output = inverse_binary(output);
	cout<<output<<endl;
	output = convert_binary_to_hex(output);
	return data.insert(data.length(), output);
}

//obtem o numero do ACK
int getacknum(string rvcpkt) {
	//extrai data
	string data = rdt_rcv(rvcpkt);
	//obtem o indice do "#" em data
	int i = data.find('#');
	//obtem o numero a partir do indice
	string num = data.substr(i+1);
	//retorna o num em inteiro
	return atoi(num.c_str());
}

void refuse_data(string data) {
	printf("(sender) refuse data: can't send pkt because buffer is full");
}

//verifica se ocorreu timeout
bool timeout() {
	//se nao foi pedido para parar o timer
	if (stop_timer == false) {
		if (/*time.time()*/now() - start_timer > TIMEOUT) {
			return true;
		}
		else {
			return false;
		}
	}
	else {
		return false;
	}
}

//socket de sender envia packet para receiver
void udt_send(string packet) {
	//sock_sender.sendto(packet, (UDP_IP_RECEIVER, UDP_PORT_RECEIVER))
	//Serial.write(packet);
}

//cria pacote
string make_pkt(int nextseqnum, string data) {
	//coloca #[numero do pacote] no final da string data e codifica
	data.insert(data.length(), "#");
	data.insert(data.end(), nextseqnum + '0');
	return data;
}

void rdt_send(string data) {
	//se o numero de pacotes a ser enviados estiver dentro do tamanho do buffer
	if (nextseqnum < base + N) {
		//cria lista de pacotes
		sndpkt.push_back(make_pkt(nextseqnum, data));
		//testa perda de pacotes do sender
		if (TEST) {
			//pacotes 10 em diante serao perdidos
			if (nextseqnum < 10) {
				udt_send(sndpkt[nextseqnum]);
			}
		}
		else {
			//envia pacote para o receiver
			udt_send(sndpkt[nextseqnum]);
		}
		printf("(sender) send %s", sndpkt[nextseqnum].c_str());
		//se buffer estiver vazio
		if (base == nextseqnum) {
			//inicia timer
			//start_timer = time.time()
			start_timer = now();
			stop_timer = false;
			printf("(sender) start timer");
		}
		//proximo numero de pacote
		nextseqnum += 1;
		printf("(sender) next sequence number: %d", nextseqnum);
		REFUSE_DATA = true;
	}
	else {
		//nao envia pacote
		if (REFUSE_DATA) {
			refuse_data(data);
			REFUSE_DATA = false;
		}
	}
}

bool corrupt(string data) {
	//get ultimos 4 digitos
	string input = extract(data);
	string cksm = data.substr(data.length()-SIZE/4, SIZE/4);
	cout<<input<<endl;
	cout<<cksm<<endl;
	if (cksm.compare(checksum(input))) {
		return false;
	}
	return true;
}

bool notcorrupt(string data) {
	return !corrupt(data);
}

//verifica se o pacote eh o esperado
bool hasseqnum(string rvcpkt, int expectedseqnum) {
	//extrai data
	string data = rdt_rcv(rvcpkt);
	//obtem o indice do "#" em data
	int i = data.find('#');
	//obtem o numero a partir do indice
	int num = atoi(data.substr(i+1).c_str());
	printf("(receiver) %s has sequence number: %d", data.c_str(), num);
	//se num for igual ao esperado
	if (num == expectedseqnum) {
		return true;
	}
	else {
		return false;
	}
}

string extract(string data) {
	return data.substr(0, data.length()-SIZE/4);
}

string rdt_rcv(string packet) {
	string data = extract(packet);
	return data;
}

/*int main(){
	string in = "012345", out;

	out = checksum(in);
	cout<<out<<endl;
	if (not_corrupt(out)) {
		cout<<"not corrupt"<<endl;
	}
}*/
