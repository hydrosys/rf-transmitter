#include <SPI.h>
#include <string.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"
#include "inetGSM.h"
#include <iostream>
#include <fstream>
#include <algorithm>
#include<pthread.h>
using namespace std;
#include <time.h>
//#include <cstdlib>
//gsm
InetGSM inet;

boolean started = false;
char smsbuffer[2000];

char n[20];

byte valor;

//SMSGSM gsm1;
char position;
char phone_num[20]; // array for the phone number string
char sms_text[100]; // array for the SMS text string
//end gsm

RF24 radio(9, 10);
// Galileo, Mega, UNO

const uint64_t pipes[3] = { 0xA0A0A0A03F,  0xF0F0F0F0AA, 0xF0F0F0F066};


boolean stringComplete = false;  // whether the string is complete
static int dataBufferIndex = 0;
boolean stringOverflow = false;
char charOverflow = 0;

char SendPayload[31] = "";
char RecvPayload[31] = "";
char serialBuffer[31] = "";

typedef enum {start, communication_error, receive_data, save_bd, save_bd2, stand_by_arduino, test, ask_for_data, ext, send_server} states;
states state = receive_data;
//states state = send_server;
typedef enum {setup_start, loop_start} states_start;
states_start state_start = setup_start;
typedef enum {setup_receive_data1, setup_receive_data2, flow_data1, flow_data2 , rcv_flow_data1, rcv_flow_data2} states_receive_data;
states_receive_data state_receive_data = setup_receive_data1;
typedef enum {setup_send_server, loop_send_server} states_send_server;
states_send_server state_send_server = setup_send_server;
bool communic1 = true;
bool communic2 = true;
unsigned long old_time = 0;
unsigned long new_time;
unsigned long tempo;
//VARIAVEIS
unsigned long t = 5000; //periodo de amostragem dos dados,
unsigned long TIMEOUT = 3000;
unsigned long NIGHT = 60000;

unsigned long duracao_noite = 2 * 60000;
unsigned long inicio;
unsigned long duracao_dia = 0.2 * 60000;
int setup_system = 1;

char s_flowRate1[40];
char s_flowRate2[40];
char n_flowRate1[40];
char n_flowRate2[40];

FILE *file, *file2, *file3;
pthread_mutex_t lock;

clock_t startt, endd;
double cpu_time_used;

bool flag = false;
bool flag2 = true;
int tentativa = 3;
bool gprs = false;
char str[999];
//gsm- file to server
ifstream file_values;
unsigned int file_size = 0;
unsigned int aux_size = 0;
unsigned int pointer = 0;

void setup(void) {
  file = fopen("train_sensor1.txt", "wt");
  if (file == 0) {
    perror("file: ");
  }
  delay(5);
  file2 = fopen("train_sensor2.txt", "wt");
  if (file2 == 0) {
    perror("file2: ");
  }

  Serial.begin(115200);

  radio.begin();

  radio.setDataRate(RF24_250KBPS);
  radio.setPALevel(RF24_PA_MAX);
  radio.setChannel(70);

  //radio.enableAckPayload();
  radio.enableDynamicPayloads();
  radio.setRetries(15, 15);
  radio.setCRCLength(RF24_CRC_16);

  radio.openWritingPipe(pipes[0]);
  radio.openReadingPipe(1, pipes[1]);
  radio.openReadingPipe(2, pipes[2]);
  radio.startListening();
  radio.setAutoAck(true);

  Serial.println();
  Serial.println("Gateway - Galileo");
  delay(500);

  inicio = millis();
  old_time = millis();

}

void loop(void)
{
  nRF_receive();
  switch (state)
  {
     case receive_data: //RECEBE DADOS
      //nRF_receive();      //espera dar timeout pra recomeçar o ciclo

      switch (state_receive_data)
      {
        case setup_receive_data1:
          //Serial.println("state = setup_receive_data1");
          serial_send("inf"); //pede pro uM 1 enviar os dados


          if (strncmp(RecvPayload, "a1", 2) == 0)
          {
            strcpy(s_flowRate1, RecvPayload);
            Serial.println("Flow rate one rcvd!");
            //state_receive_data = rcv_flow_data1;
            strcpy(n_flowRate1, s_flowRate1 + 2);
            strcpy(s_flowRate1, "invalid");
            state = save_bd;
            flag = false;
            communic1 = true;
            

          }
          else
          {
             state = receive_data;
             state_receive_data = setup_receive_data2;            
          }

          //se nao receber nada por 3s envia inf novamente
           /* else if (timeout(TIMEOUT)) {
            //state_receive_data = setup_receive_data1;
            communic1 = false;
            state = receive_data;
            state_receive_data = setup_receive_data2;
            Serial.println("TIMEOUT SENSOR 1");
            flag = false;
            old_time = millis();
          }*/
          break;


        case setup_receive_data2:
          //Serial.println("state = setup_receive_data2");
          serial_send("inf2"); //pede pro uM 2 enviar os dados

          if (strncmp(RecvPayload, "a2", 2) == 0)
          {
            strcpy(s_flowRate2, RecvPayload);
            Serial.println("Flow rate two rcvd!");
            //state_receive_data = rcv_flow_data2;
            strcpy(n_flowRate2, s_flowRate2 + 2);
            strcpy(s_flowRate2, "invalid");
            state = save_bd2;
            flag = false;
            communic2 = true;

          }
          else
          {
            state = receive_data;
            state_receive_data = setup_receive_data1;            
          }
          

          //se nao receber nada por 3s envia inf novamente
          /*else if (timeout(TIMEOUT)) {
            //state_receive_data = setup_receive_data2;
            communic2 = false;
            state = receive_data;
            state_receive_data = setup_receive_data1;
            Serial.println("TIMEOUT SENSOR 2");
            flag = false;
            old_time = millis();
          }*/
          //state = test;
          break;

      }

      break;

    case save_bd: //SALVA NO BD

      Serial.println("state = save_bd");
      fprintf(file, "%s\n", n_flowRate1);

      state = receive_data;

      state_receive_data = setup_receive_data2;

      //Checa se ja acabou o periodo de uma noite
      if ((millis() - inicio) > duracao_noite)
      {
        state = ext;

      }
      old_time = millis();

      break;

    case save_bd2: //SALVA NO BD 2
      Serial.println("state = save_bd2");
      fprintf(file2, "%s\n", n_flowRate2);

      
      state = receive_data;
      state_receive_data = setup_receive_data1;


      //Checa se ja acabou o periodo de uma noite
      if ((millis() - inicio) > duracao_noite)
      {
        state = ext;

      }

      old_time = millis();


      break;

    case stand_by_arduino: //STANDY-BY ARDUINO
      Serial.println("state = stand_by_arduino");
      //FAZ O QUE TEM QUE FAZER PRA BOTAR O ARDUINO EM STAND-BY
      state = ext;
      break;

    case test: //TESTES

      Serial.println("EXIT");
      delay(5000);

      break;



    case ext: //COMECOU O DIA, ESPERA O PROXIMO CICLO DA NOITE
      Serial.println("state = ext");
      inicio = millis(); //inicio do dia
      //fecha o arquivo da noite anterior
      fclose(file);
      fclose(file2);


      delay(1000); //tempo pra fechar o arquivo
      if (setup_system == 0) //caso seja o primeiro ciclo, não vai ter arquivo de teste ainda
      {
        startt = clock(); //Calcula o tempo que a função leva para executar
        pthread_mutex_lock(&lock);
        system("rm results.txt");
        //delay(100);
                
        system("python anomaly_detection.py");
      
        pthread_mutex_unlock(&lock);
        endd = clock();
        cpu_time_used = ((double) (endd - startt)) / CLOCKS_PER_SEC;
        Serial.print("Sec Analise: ");
        Serial.println(cpu_time_used);
      }
      else
      {
        setup_system = 0;
      }
    

      while ((millis() - inicio) < duracao_dia)
      {
        if (!flag) {
          Serial.println("Esperando anoitecer!!!!!!");
          flag = true;
        }
      }
      flag = false;
      if (!flag2) {
        state = send_server;
        state_send_server = setup_send_server;
        
      }
      else {
        state = receive_data;
        state_receive_data = setup_receive_data1;
        flag2 = false;
      }
      //state = test;
      //depois da primeira noite, o resto é checagem
      file = fopen("teste_sensor1.txt", "wt");
      file2 = fopen("teste_sensor2.txt", "wt");
      //communic1 = false;
      //communic2 = false;


      break;

    case send_server:

      switch (state_send_server) {

        case setup_send_server:

          Serial.println(F("Testando GSM Shield SIM900"));
          if (gsm.begin(9600))
          {
            Serial.println(F("\nstatus=READY"));
            started = true;
            state_send_server = loop_send_server;
          }
          else Serial.println(F("\nstatus=IDLE"));
          break;

        case loop_send_server:
          //Serial.println("in loop");
          if (started)
          {
            file_values.open("results.txt", ios::in | ios::binary | ios::ate);
            if (file_values.good()) {
              file_values.seekg(0, ios::end);
              file_size = file_values.tellg();
              Serial.print("size of file in bytes is ");
              Serial.println(file_size);
              file_values.seekg(0, ios::beg);


              file_values.read(smsbuffer, file_size);
              file_values.close();
              //smsbuffer[aux_size] = '\0';
              smsbuffer[file_size] = '\0';
              
              startt = clock(); //Calcula o tempo que a função leva para executar
              envia_GSM(smsbuffer);
              endd = clock();
              cpu_time_used = ((double) (endd - startt)) / CLOCKS_PER_SEC;
              Serial.print("Sec GSM: ");
              Serial.println(cpu_time_used);
              
              Serial.println("before delay 15s");
              delay(10000);
              Serial.println("after delay 15s");


              inicio = millis(); //inicio da noite
              old_time = millis();
              state = receive_data;
              state_receive_data = setup_receive_data1;

              //state = test;
            }
            else {
              Serial.println("Arquivo não encontrado");
            }
          }

          break;
      }
  }
} // end loop()



void envia_GSM(char* texto)
{
  Serial.println("envia_GSM");
  //char temp_string[55];
  char msg[1000];
  char tmp[2000];
  int numdata = 0;
  int tries = 1;
    if (!gprs) {
      inet.attachGPRS("claro.com.br", "claro", "claro");
      gprs = true;
      Serial.println(F("status=Conectado..."));
    }
//  if (inet.attachGPRS("claro.com.br", "claro", "claro"))
//    Serial.println(F("status=Conectado..."));
//  else Serial.println(F("status=Nao conectado !!"));
  delay(100);
  if (gprs){
//    String str(smsbuffer);
//    String valor = "MSG_Texto1="+str;
//    replace(0,valor.length(),'\n',' ');
//    valor.toCharArray(tmp, valor.length()+1);
    strcpy(tmp,"MSG_Texto1=");
    strcat(tmp,smsbuffer);
//    string str(tmp);
//    std::replace(str.begin(),str.end(),'\n',' ');
//    str.toCharArray(tmp,str.length()+1);
//    for(int i=0;i<strlen(tmp);i++){
//      if(tmp[i]=='\n')tmp[i]=' ';
//      if(i==1400)tmp[i]='\0';
//    }
    
    Serial.println(tmp);
    Serial.println(strlen(tmp));
    Serial.println("httpPOST");
    numdata = inet.httpPOST("hydrosys.net16.net", 80, "/add.php", tmp, msg, 1000);
    Serial.print("numdata: ");
    Serial.println(numdata);
    Serial.println(msg);
    delay(5000);
    Serial.println("after delay 5s");
    //inet.dettachGPRS();
    Serial.println("after dettach");
  }
}

bool timeout(unsigned long t) {
  new_time = millis();
  //Serial.print("time: ");
  Serial.println(new_time - old_time);
  if (new_time - old_time > t) {
    old_time = new_time;
    return true;
  }
  return false;
}


void nRF_receive()
{

  int len = 0;
  if ( radio.available() )
  {

    while ( radio.available() ) {
      len = radio.getDynamicPayloadSize();
      radio.read(&RecvPayload, len);
      delay(5);
    }

    RecvPayload[len] = 0; // null terminate string

    Serial.print("R:");

    Serial.print(RecvPayload);
    Serial.println();

  }
  else {
    delay(5);
  }

} // end nRF_receive()



void serial_send(char* str) {

  //radio.stopListening();

  SendPayload[0] = 0;
  strcat(SendPayload, str);

  radio.stopListening();

  bool ok = radio.write(&SendPayload, strlen(SendPayload));
  //Serial.print("Ok? ");
  //Serial.println(ok);
  Serial.print("S:");
  Serial.println(SendPayload);
  stringComplete = false;


  SendPayload[0] = 0;
  dataBufferIndex = 0;

  radio.startListening();
}
