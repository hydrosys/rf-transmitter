#Abrir o arquivo de treinamento
import statistics as st
import math as mt

X_train = []
X_test = []
X_train2 = []
X_test2 = []
i=0

with open('train_sensor1.txt') as f1:
    lines = f1.read().splitlines()
    for row in lines:
    	if(row != ""):
    		X_train.append(float(row))

with open('teste_sensor1.txt') as f2:
    lines = f2.read().splitlines()
    for row in lines:
    	if(row != ""):
    		X_test.append(float(row)) 

with open('train_sensor2.txt') as f3:
    lines = f3.read().splitlines()
    for row in lines:
    	if(row != ""):
    		X_train2.append(float(row))

with open('teste_sensor2.txt') as f4:
    lines = f4.read().splitlines()
    for row in lines:
    	if(row != ""):
    		X_test2.append(float(row))      	     	  	

f1.close()
f2.close()
f3.close()
f4.close()

mu  = 0
dev = 0
classe_pred = []
normal = 0
vazamento = 0
Probability1 = 0

#treina sensor 1
if(X_train):
	mu = st.mean(X_train)
	dev = st.stdev(X_train)

#testa Sensor 1
for i in range(len(X_test)):
	#vazamento
	if( (X_test[i] > (mu + 2*dev)) or (X_test[i] < (mu - 2*dev)) ):
		classe_pred.append(1)
		vazamento += 1
	else: #normal
		classe_pred.append(0)
		normal += 1
#caso o arquivo de testes esteja vazio
if(vazamento == 0 and normal == 0):
	normal =1

Probability1 = mt.ceil(float(vazamento)*100/(vazamento+normal))
print("sensor 1:")
print(Probability1)
#treina sensor 2
if(X_train2 != []):
	mu2 = st.mean(X_train2)
	dev2 = st.stdev(X_train2)

classe_pred2 = []
normal = 0
vazamento = 0
Probability2 = 0


#testa sensor 2
for i in range(len(X_test2)):
	#vazamento
	if( (X_test2[i] > (mu2 + 2*dev2)) or (X_test2[i] < (mu2 - 2*dev2)) ):
		classe_pred2.append(1)
		vazamento += 1

	else: #normal
		classe_pred2.append(0)
		normal += 1
if(vazamento == 0 and normal ==0):
	normal =1
#caso o arquivo de testes esteja vazio		
Probability2 = mt.ceil(float(vazamento)*100/(vazamento+normal))
print("sensor 2:")
print(Probability2)

file = open('results.txt', 'w')

file.write('%s' % "s1")
file.write("\n")

if(Probability1 >= 50):
	print "Probabilidade de vazamento alta no sensor 1!"
	print Probability1
	file.write('%.2f' % Probability1)
	file.write("\n")
else:
	file.write('%d' % 0)
	file.write("\n")	
#Escreve alguns valores de fluxo do sensor 1 no arquivo txt
#X1_amostras = X_test[::2]
X1_amostras = []
for i in range(len(X_test)):
	if(i % 3 == 0):
		X1_amostras.append(int(X_test[i]))

for i in range(len(X1_amostras)):
	file.write('%d' % X1_amostras[i])
	file.write("\n")

file.write('%s' % "s2")
file.write("\n")

if(Probability2 >= 50):
	print "Probabilidade de vazamento sensor 2!"
	print Probability2
	file.write('%.2f' % Probability2)
	file.write("\n")
else:
	file.write('%d' % 0)
	file.write("\n")	
#Escreve alguns valores de fluxo do sensor 1 no arquivo txt
#X2_amostras = X_test2[::2]
X2_amostras = []
for i in range(len(X_test2)):
	if(i % 3 == 0):
		X2_amostras.append(int(X_test2[i]))

for i in range(len(X2_amostras)):
	file.write('%d' % X2_amostras[i])
	file.write("\n")

file.write("end");
file.close();





