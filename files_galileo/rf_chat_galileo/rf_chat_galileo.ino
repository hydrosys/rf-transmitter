#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"

//RF24 radio(40,53);
RF24 radio(9, 10);

const uint64_t pipes[2] = { 0xDEDEDEDEE7LL, 0xDEDEDEDEE9LL };

boolean stringComplete = false;  // whether the string is complete
static int dataBufferIndex = 0;
boolean stringOverflow = false;
char charOverflow = 0;

char SendPayload[31] = "";
char RecvPayload[31] = "";
char serialBuffer[31] = "";

typedef enum {start, communication_error, receive_data, save_bd, stand_by_arduino, test} states;
states state = test;
typedef enum {setup_start,loop_start} states_start;
states_start state_start = setup_start;
bool communication = false;
unsigned long old_time;
unsigned long new_time;
unsigned long TIMEOUT = 10000;
bool flag = true;
//char snd[] = "snd";
//char rcv[] = "rcv";

void setup(void) {

  Serial.begin(115200);

  Serial.println("RF Chat V0.90");

  //printf_begin();
  radio.begin();

  radio.setDataRate(RF24_250KBPS);
  radio.setPALevel(RF24_PA_MAX);
  radio.setChannel(70);

  radio.enableDynamicPayloads();
  radio.setRetries(15, 15);
  radio.setCRCLength(RF24_CRC_16);

  radio.openWritingPipe(pipes[0]);
  radio.openReadingPipe(1, pipes[1]);

  radio.startListening();
  radio.printDetails();

  Serial.println();
  Serial.println("RF Chat V0.90");
  delay(500);

}

void loop(void) {
  switch (state) {
    case start:
      switch (state_start) {
        case setup_start:     
          //envia snd para o arduino   
          serial_send("snd");
          //inicializa o time
          new_time = millis();
          old_time = new_time;
          //muda para o estado loop_start
          state_start = loop_start;
          break;
        case loop_start:          
          //tempo recente
          new_time = millis();
          Serial.print("time: ");
          Serial.println(new_time - old_time);
          //chama test_communication
          if (test_communication()) {
            state = receive_data;
            Serial.println("state = receive_data;");
          }
          //compara a diferença de tempo com o TIMEOUT
          else if (new_time - old_time > TIMEOUT) {
            state = communication_error;
            Serial.println("state = communication_error;");
            Serial.println("communication error: TIMEOUT");
          }
          break;
      }
      break;
    case communication_error:
      //envia error para o servidor
      break;
    case receive_data:
      nRF_receive();
      break;
    case save_bd:
      break;
    case stand_by_arduino:
      
      break;
    case test:
      serial_send("wup");
      break;
  }
} // end loop()

void awake_arduino() {

}
//se o que recebeu foi rcv
bool test_communication() {
  bool result = false;

  nRF_receive();
  if (strcmp(RecvPayload,"rcv") == 0) {
    result = true;
  }
  
  return result;
}

void serialEvent() {
  while (Serial.available() > 0 ) {
    char incomingByte = Serial.read();

    if (stringOverflow) {
      serialBuffer[dataBufferIndex++] = charOverflow;  // Place saved overflow byte into buffer
      serialBuffer[dataBufferIndex++] = incomingByte;  // saved next byte into next buffer
      stringOverflow = false;                          // turn overflow flag off
    } else if (dataBufferIndex > 31) {
      stringComplete = true;        // Send this buffer out to radio
      stringOverflow = true;        // trigger the overflow flag
      charOverflow = incomingByte;  // Saved the overflow byte for next loop
      dataBufferIndex = 0;          // reset the bufferindex
      break;
    }
    else if (incomingByte == '\n') {
      serialBuffer[dataBufferIndex] = 0;
      stringComplete = true;
    } else {
      serialBuffer[dataBufferIndex++] = incomingByte;
      serialBuffer[dataBufferIndex] = 0;
    }
  } // end while()
} // end serialEvent()

void nRF_receive(void) {
  int len = 0;
  if ( radio.available() ) {
    bool done = false;
    while ( radio.available() ) {
      len = radio.getDynamicPayloadSize();
      radio.read(&RecvPayload, len);
      delay(5);
    }

    RecvPayload[len] = 0; // null terminate string

    //lcd.setCursor(0,0);
    //lcd.print("R:");
    Serial.print("R:");
    //lcd.setCursor(2,0);
    //lcd.print(RecvPayload);
    Serial.print(RecvPayload);
    Serial.println();
    //RecvPayload[0] = 0;  // Clear the buffers
  }

} // end nRF_receive()

void serial_receive(void) {

  if (stringComplete) {
    strcat(SendPayload, serialBuffer);
    // swap TX & Rx addr for writing
    radio.openWritingPipe(pipes[1]);
    radio.openReadingPipe(0, pipes[0]);
    radio.stopListening();
    bool ok = radio.write(&SendPayload, strlen(SendPayload));

    //lcd.setCursor(0,1);
    //lcd.print("S:");
    Serial.print("S:");
    //lcd.setCursor(2,1);
    //lcd.print(SendPayload);
    Serial.print(SendPayload);
    Serial.println();
    stringComplete = false;

    // restore TX & Rx addr for reading
    radio.openWritingPipe(pipes[0]);
    radio.openReadingPipe(1, pipes[1]);
    radio.startListening();
    SendPayload[0] = 0;
    dataBufferIndex = 0;
  } // endif
} // end serial_receive()

void serial_send(char* str) {

  strcat(SendPayload,str);
  // swap TX & Rx addr for writing
  radio.openWritingPipe(pipes[1]);
  radio.openReadingPipe(0, pipes[0]);
  radio.stopListening();
  bool ok = radio.write(&SendPayload, strlen(SendPayload));

  //lcd.setCursor(0,1);
  //lcd.print("S:");
  Serial.print("S:");
  //lcd.setCursor(2,1);
  //lcd.print(SendPayload);
  Serial.print(SendPayload);
  Serial.println();
  stringComplete = false;

  // restore TX & Rx addr for reading
  radio.openWritingPipe(pipes[0]);
  radio.openReadingPipe(1, pipes[1]);
  radio.startListening();
  SendPayload[0] = 0;
  dataBufferIndex = 0;
} // end serial_receive()
