#include <SPI.h>
#include <string.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"
#include "inetGSM.h"
#include <iostream>
#include <fstream>
#include <algorithm>
#include<pthread.h>
using namespace std;
#include <time.h>
//#include <cstdlib>
//gsm
InetGSM inet;

boolean started = false;
char smsbuffer[2000];

char n[20];
char cmd[10];
char aux[10];

byte valor;

//SMSGSM gsm1;
char position;
char phone_num[20]; // array for the phone number string
char sms_text[100]; // array for the SMS text string
//end gsm

RF24 radio(9, 10);
// Galileo, Mega, UNO

const uint64_t pipes[3] = { 0xA0A0A0A03F,  0xF0F0F0F0AA, 0xF0F0F0F066};


boolean stringComplete = false;  // whether the string is complete
static int dataBufferIndex = 0;
boolean stringOverflow = false;
char charOverflow = 0;

char SendPayload[31] = "";
char RecvPayload[31] = "";
char serialBuffer[31] = "";

typedef enum {start, communication_error, receive_data, save_bd, save_bd2, stand_by_arduino, test, ask_for_data, ext, send_server} states;
states state = receive_data;
typedef enum {setup_start, loop_start} states_start;
states_start state_start = setup_start;
typedef enum {setup_receive_data1, setup_receive_data2, flow_data1, flow_data2 , rcv_flow_data1, rcv_flow_data2} states_receive_data;
states_receive_data state_receive_data = setup_receive_data1;
typedef enum {setup_send_server, loop_send_server} states_send_server;
states_send_server state_send_server = setup_send_server;
unsigned long old_time = 0;
unsigned long new_time;

//VARIAVEIS

unsigned long interval = 0;
unsigned long duracao_treino = 4 * 60000;
unsigned long duracao_teste = 1 * 60000;
unsigned long duracao_noite = duracao_treino; //comeca treinando
unsigned long duracao_dia = 1 * 60000;
unsigned long inicio;
int setup_system = 1;

char s_flowRate1[40];
char s_flowRate2[40];
char n_flowRate1[40];
char n_flowRate2[40];
FILE *file, *file2, *file3, *f = NULL;
//pthread_mutex_t lock;

clock_t startt, endd;
double cpu_time_used;

bool retreina = false;
bool flag = false;
bool gprs = false;
char str[999];
//gsm- file to server
ifstream file_values;
unsigned int file_size = 0;
unsigned int aux_size = 0;
unsigned int pointer = 0;


int contONE = 0;
int contTWO = 0;

void setup(void) {
  file = fopen("train_sensor1.txt", "wt");
  if (file == 0) {
    perror("file: ");
  }
  delay(5);
  file2 = fopen("train_sensor2.txt", "wt");
  if (file2 == 0) {
    perror("file2: ");
  }

  Serial.begin(115200);

  radio.begin();

  radio.setDataRate(RF24_250KBPS);
  radio.setPALevel(RF24_PA_MAX);
  radio.setChannel(70);

  //radio.enableAckPayload();
  radio.enableDynamicPayloads();
  radio.setRetries(15, 15);
  radio.setCRCLength(RF24_CRC_16);

  radio.openWritingPipe(pipes[0]);
  radio.openReadingPipe(1, pipes[1]);
  radio.openReadingPipe(2, pipes[2]);
  radio.startListening();
  radio.setAutoAck(true);

  Serial.println();
  Serial.println("Gateway - Galileo");
  delay(500);

 
  Serial.println(F("Testando GSM Shield SIM900"));
  if (gsm.begin(9600))
  {
    Serial.println(F("\nstatus=READY"));
    started = true;
    //state_send_server = loop_send_server;
  }
  else Serial.println(F("\nstatus=IDLE"));

  inicio = millis();
  old_time = millis();
  Serial.println("Setup finalizado");

}

void loop(void)
{
  nRF_receive();
  switch (state)
  {
    case receive_data: //RECEBE DADOS

      if (timeout(interval)) {
        serial_send("inf"); //pede pro uM  enviar os dados
        //Serial.println("inf");
      }


      if (strncmp(RecvPayload, "a1", 2) == 0)
      {
        strcpy(s_flowRate1, RecvPayload);
        Serial.println("Flow rate one rcvd!");
        strcpy(n_flowRate1, s_flowRate1 + 2);
        strcpy(s_flowRate1, "invalid");
        state = save_bd;
        flag = false;

        contONE++;
        //strcpy(RecvPayload, " ");
        RecvPayload[0] = 0;
      }
      else if (strncmp(RecvPayload, "a2", 2) == 0)
      {
        strcpy(s_flowRate2, RecvPayload);
        Serial.println("Flow rate two rcvd!");
        strcpy(n_flowRate2, s_flowRate2 + 2);
        strcpy(s_flowRate2, "invalid");
        state = save_bd2;
        flag = false;

        contTWO++;
        //strcpy(RecvPayload, " ");
        RecvPayload[0] = 0;
      }

      break;
    //----------------------------------------------------------------------------------------------------------------------------------------
    case save_bd: //SALVA NO BD

      Serial.println("state = save_bd");
      fprintf(file, "%s\n", n_flowRate1);

      state = receive_data;

      //Checa se ja acabou o periodo de uma noite
      if ((millis() - inicio) > duracao_noite)
      {
        state = ext;

      }
      Serial.print("ContONE: ");
      Serial.println(contONE);
      Serial.print("ContTWO: ");
      Serial.println(contTWO);
      old_time = millis();

      break;
    //-------------------------------------------------------------------------------------------------------------------------------------
    case save_bd2: //SALVA NO BD 2
      Serial.println("state = save_bd2");
      fprintf(file2, "%s\n", n_flowRate2);

      state = receive_data;


      //Checa se ja acabou o periodo de uma noite
      if ((millis() - inicio) > duracao_noite)
      {
        state = ext;

      }
      Serial.print("ContONE: ");
      Serial.println(contONE);
      Serial.print("ContTWO: ");
      Serial.println(contTWO);

      old_time = millis();


      break;

    //-------------------------------------------------------------------------------------------------------------------
    case test: //TESTES

      Serial.println("EXIT");
      delay(5000);

      break;
    //-------------------------------------------------------------------------------------------------------------------

    case ext: //COMECOU O DIA, ESPERA O PROXIMO CICLO DA NOITE
      Serial.println("state = ext");
      inicio = millis(); //inicio do dia
      //fecha o arquivo da noite anterior

      fclose(file);
      fclose(file2);
    
      
      //delay(1000); //tempo pra fechar o arquivo
      if (!setup_system) //caso seja o primeiro ciclo, não vai ter arquivo de teste ainda
      { 
        //startt = clock(); //Calcula o tempo que a função leva para executar
        system("rm results.txt");
        system("rm acabou.txt");
        system("python anomaly_detection.py");
        
        while (f == NULL)
        {
          f = fopen("acabou.txt", "r");
        }
        fclose(f);
        f = NULL;
        Serial.println("DEBUG");
        //endd = clock();
        //cpu_time_used = ((double) (endd - startt)) / CLOCKS_PER_SEC;
        //Serial.print("Sec Analise: ");
        //Serial.println(cpu_time_used);
        
         //O proximo estado vai ser mandar as informacoes pro servidor
        state = send_server;
        state_send_server = setup_send_server;
      }
      else
      {
        setup_system = 0;
        state = receive_data;           
      }
      Serial.println();
      Serial.println("FAZENDO A ANALISE DOS DADOS");
      Serial.println();
    
      while ((millis() - inicio) < duracao_dia)
      {
        //broadcast avisando que anoiteceu
        if ((millis() - inicio) < duracao_dia / 3)
        {
          serial_send("s");
        }

        if (!flag) {
          Serial.println("Esperando anoitecer!!!!!!");
          flag = true;
        }
      }
      flag = false;


      //re-treinar
      Serial.println("Check new command");
      httpGET();
      Serial.println("after httpGET()");
      Serial.println(cmd);
      Serial.println(strcmp(cmd, "trn"));
      if (strcmp(cmd, "trn") == 0)
      {
        Serial.println("Retreina!");
        //state = retrain;
        file = fopen("train_sensor1.txt", "wt");
        if (file == 0) {
          perror("file: "); 
        }
        delay(5);
        file2 = fopen("train_sensor2.txt", "wt");
        if (file2 == 0) {
          perror("file2: ");
        }
        //As duas variaveis indicam que precisa treinar de novo (Sim, precisa das duas)
        duracao_noite = duracao_treino;
        setup_system = 1; 
        retreina = true;
        httpPOST();
        state = receive_data; //manda pegar os dados de novo
      }
      else if (strncmp(cmd, "tmp", 3) == 0) { //Muda periodo de amostragem
        Serial.println("Change period!");
        //strcpy(aux, cmd + 3);
        char* pEnd;
        interval = strtol(cmd+3,&pEnd,10);
        Serial.print("interval=");
        Serial.println(interval);
        httpPOST();
      }


      if(!retreina) //Caso nao precise retreinar, pega inf pro arquivo de teste
      { 
        file = fopen("teste_sensor1.txt", "wt");
        file2 = fopen("teste_sensor2.txt", "wt"); 
        duracao_noite = duracao_teste;
       
      }
      else  
      {
           retreina = false;
      }


      inicio = millis();
      contONE = 0;
      contTWO = 0;

      break;
    //---------------------------------------------------------------------------------------------------------------------------------------------------------
    case send_server:

      switch (state_send_server) {

        case setup_send_server:
          //
          //          Serial.println(F("Testando GSM Shield SIM900"));
          //          if (gsm.begin(9600))
          //          {
          //            Serial.println(F("\nstatus=READY"));
          //            started = true;
          state_send_server = loop_send_server;
          //          }
          //          else Serial.println(F("\nstatus=IDLE"));
          break;

        case loop_send_server:
          //Serial.println("in loop");
          if (started)
          {
            file_values.open("results.txt", ios::in | ios::binary | ios::ate);
            if (file_values.good()) {
              file_values.seekg(0, ios::end);
              file_size = file_values.tellg();
              Serial.print("size of file in bytes is ");
              Serial.println(file_size);
              file_values.seekg(0, ios::beg);


              file_values.read(smsbuffer, file_size);
              file_values.close();
              //smsbuffer[aux_size] = '\0';
              smsbuffer[file_size] = '\0';

              startt = clock(); //Calcula o tempo que a função leva para executar
              envia_GSM(smsbuffer);
              endd = clock();
              cpu_time_used = ((double) (endd - startt)) / CLOCKS_PER_SEC;
              Serial.print("Sec GSM: ");
              Serial.println(cpu_time_used);

              Serial.println("before delay 15s");
              delay(10000);
              Serial.println("after delay 15s");


              inicio = millis(); //inicio da noite
              old_time = millis();
              state = receive_data;
            }
            else {
              Serial.println("Arquivo não encontrado");
            }
          }

          break;
      }
  }
} // end loop()

//---------------------------------------------------------------------------------------------------------------------------------------------

void httpPOST()
{
  char temp_string[100] = "text=ok&sender=galileo00&receiver=website";
  char msg[10];
  int numdata;
  if (inet.attachGPRS("claro.com.br", "claro", "claro"))
    Serial.println(F("status=Conectado..."));
  else Serial.println(F("status=Nao conectado !!"));
  delay(100);
  numdata = inet.httpPOST("hydrosys.net16.net", 80, "/post.php", temp_string, msg, 50);
  Serial.print("numdata: ");
  Serial.println(numdata);
  //delay(5000);
}

void httpGET()
{
  char msg[1000];
  char *pch;
  int numdata;
  int i;
  if (inet.attachGPRS("claro.com.br", "claro", "claro"))
    Serial.println(F("status=Conectado..."));
  else Serial.println(F("status=Nao conectado !!"));
  delay(100);
  numdata = inet.httpGET("hydrosys.net16.net", 80, "/log.html", msg, 1000);
  Serial.print("numdata: ");
  Serial.println(numdata);
  //Serial.print("msg: ");
  //Serial.println(msg);
  //put the command in the cmd
  pch = strstr(msg, "text: ");
  if (pch) {
    strncpy(cmd, pch + 6, 10); //where 10 is the size of the command
    for (i=0;i<10;i++) {
      if (cmd[i]==' ' || cmd[i]=='\r') {
        cmd[i] = 0;
      }
    }
    Serial.print("cmd: ");
    Serial.println(cmd);
    //delay(5000);
  }
}

void envia_GSM(char* texto)
{
  Serial.println("envia_GSM");
  //char temp_string[55];
  char msg[1000];
  char tmp[2000];
  int numdata = 0;
  int tries = 1;
  if (!gprs) {
    inet.attachGPRS("claro.com.br", "claro", "claro");
    gprs = true;
    Serial.println(F("status=Conectado..."));
  }
  //  if (inet.attachGPRS("claro.com.br", "claro", "claro"))
  //    Serial.println(F("status=Conectado..."));
  //  else Serial.println(F("status=Nao conectado !!"));
  delay(100);
  if (gprs) {
    //    String str(smsbuffer);
    //    String valor = "MSG_Texto1="+str;
    //    replace(0,valor.length(),'\n',' ');
    //    valor.toCharArray(tmp, valor.length()+1);
    strcpy(tmp, "MSG_Texto1=");
    strcat(tmp, smsbuffer);
    //    string str(tmp);
    //    std::replace(str.begin(),str.end(),'\n',' ');
    //    str.toCharArray(tmp,str.length()+1);
    //    for(int i=0;i<strlen(tmp);i++){
    //      if(tmp[i]=='\n')tmp[i]=' ';
    //      if(i==1400)tmp[i]='\0';
    //    }

    Serial.println(tmp);
    Serial.println(strlen(tmp));
    Serial.println("httpPOST");
    numdata = inet.httpPOST("hydrosys.net16.net", 80, "/add.php", tmp, msg, 1000);
    Serial.print("numdata: ");
    Serial.println(numdata);
    Serial.println(msg);
    delay(5000);
    Serial.println("after delay 5s");
    //inet.dettachGPRS();
    Serial.println("after dettach");
  }
}
//--------------------------------------------------------------------------------------------------------------------------------------------------------
bool timeout(unsigned long t) {
  new_time = millis();
  //Serial.print("time: ");
  //Serial.println(new_time - old_time);
  if (new_time - old_time > t) {
    old_time = new_time;
    return true;
  }
  return false;
}
//---------------------------------------------------------------------------------------------------------------------------------------------------------

void nRF_receive()
{

  int len = 0;
  if ( radio.available() )
  {

    while ( radio.available() ) {
      len = radio.getDynamicPayloadSize();
      radio.read(&RecvPayload, len);
      delay(5);
    }

    RecvPayload[len] = 0; // null terminate string

    Serial.print("R:");

    Serial.print(RecvPayload);
    Serial.println();

  }
  else {
    delay(5);
  }

} // end nRF_receive()

//------------------------------------------------------------------------------------------------------------------------------------------------------------------

void serial_send(char* str) {

  //radio.stopListening();

  SendPayload[0] = 0;
  strcat(SendPayload, str);

  radio.stopListening();

  bool ok = radio.write(&SendPayload, strlen(SendPayload));
  //Serial.print("Ok? ");
  //Serial.println(ok);
  //Serial.print("S:");
  //Serial.println(SendPayload);
  stringComplete = false;


  SendPayload[0] = 0;
  dataBufferIndex = 0;

  radio.startListening();
}
