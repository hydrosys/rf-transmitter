#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"

//RF24 radio(40,53);
RF24 radio(9, 10);

const uint64_t pipes[2] = { 0xDEDEDEDEE7LL, 0xDEDEDEDEE9LL };

boolean stringComplete = false;  // whether the string is complete
static int dataBufferIndex = 0;
boolean stringOverflow = false;
char charOverflow = 0;

char SendPayload[31] = "";
char RecvPayload[31] = "";
char serialBuffer[31] = "";

typedef enum {start, communication_error, receive_data, save_bd, stand_by_arduino, test, ask_for_data, ext} states;
states state = start;
typedef enum {setup_start,loop_start} states_start;
states_start state_start = setup_start;
typedef enum {setup_receive_data,pression_data,wait_receive_data,flow_data,rcv_pression_data,rcv_flow_data} states_receive_data;
states_receive_data state_receive_data = setup_receive_data;
bool communication = false;
unsigned long old_time = 0;
unsigned long new_time;
unsigned long tempo;
unsigned long t = 10000; //periodo de amostragem dos dados, VARIAVEL
unsigned long TIMEOUT = 1000; //deve ser maior ou igual a 1 segundo
unsigned long INVALID = 1000000;
unsigned long NIGHT = 60000;
unsigned long int s_pressao;
unsigned long int s_flowRate;
bool flag = true;
//char snd[] = "snd";
//char rcv[] = "rcv";

FILE *file;

void setup(void) {
  file = fopen("database.txt","w");
  if(file == NULL)
  {
    printf("Error opening file\n");
    exit(1);
  }
  
  Serial.begin(115200);

  Serial.println("RF Chat V0.90");

  //printf_begin();
  radio.begin();

  radio.setDataRate(RF24_250KBPS);
  radio.setPALevel(RF24_PA_MAX);
  radio.setChannel(70);

  radio.enableDynamicPayloads();
  radio.setRetries(15, 15);
  radio.setCRCLength(RF24_CRC_16);

  radio.openWritingPipe(pipes[0]);
  radio.openReadingPipe(1, pipes[1]);

  radio.startListening();
  radio.printDetails();

  Serial.println();
  Serial.println("RF Chat V0.90");
  delay(500);

}

void loop(void) {
  switch (state) {
    case start:
      switch (state_start)
      {
        case setup_start:     
          //envia snd para o arduino   
          serial_send("snd");
          //inicializa o time
          new_time = millis();
          old_time = new_time;
          //muda para o estado loop_start
          state_start = loop_start;
          break;
          
        case loop_start:          
          //tempo recente
          new_time = millis();
          Serial.print("time: ");
          Serial.println(new_time - old_time);
          //chama test_communication
          if (test_communication()) 
          {
            state = ask_for_data;
            Serial.println("state = ask_for_data;");
            old_time = millis();
          }
          //compara a diferença de tempo com o TIMEOUT
          else if (new_time - old_time > TIMEOUT) 
          {
            state = communication_error;
            Serial.println("state = communication_error;");
            Serial.println("communication error: TIMEOUT");
          }
          break;
      }
      break;
    case communication_error:
      //envia error para o servidor
      break;
    case receive_data:
      //nRF_receive();
      switch(state_receive_data){
        case setup_receive_data:
          serial_send("inf"); //pede pro arduino enviar os dados
          state_receive_data = pression_data;
          break;
        case wait_receive_data:
          //pega os dados:
          //delay(1000);
          //state_receive_data = pression_data;
          nRF_receive();
          if (strcmp(RecvPayload,"rcv inf") == 0) {
            state_receive_data = pression_data;
          }
          //se der time out volta ao estado anterior
          if (timeout()) {
            state_receive_data = setup_receive_data;
          }
          break;
        case pression_data:        
          s_pressao = INVALID;
          s_pressao = uli_receive();
          if (s_pressao != INVALID) {
            state_receive_data = rcv_pression_data;
          }
          break;
        case rcv_pression_data:        
          Serial.println("rcv_pression_data");
          serial_send("rcv p");
          state_receive_data = flow_data;
          break;
        case flow_data:
          Serial.println("flow_data");
          s_flowRate = INVALID;
          s_flowRate = uli_receive();
          if (s_flowRate != INVALID) {
            state_receive_data = rcv_flow_data;
          }
          break;
        case rcv_flow_data:
          serial_send("rcv f");
          nRF_receive();
          if (timeout()) {
            state = save_bd;
          }
          //state = ext;
          Serial.print("Pressao: ");
          Serial.println(s_pressao);
          Serial.print("Fluxo: ");
          Serial.println(s_flowRate);
          break;
      }
      //delay(1000);
      //s_flowRate = uli_receive();
      //delay(1000);
      //state = save_bd;
      break;
      
    case save_bd:
      //fprintf(file,"%lu %lu\n",s_pressao,s_flowRate);
      state = ask_for_data;
      state_receive_data = setup_receive_data;
      break;
      
    case stand_by_arduino:
      
      break;
      
    case test:
      //serial_send("wup");
      state = receive_data;
      break;

    case ask_for_data:
      tempo = millis() - old_time;

      if(tempo > t)
      {
        state = receive_data;
        old_time = millis();
      }
      
      break;
    case ext:
      fclose(file);
      break;
  }
} // end loop()

bool timeout() {
  new_time = millis();
  if (new_time - old_time > TIMEOUT) {
    old_time = new_time;
    return true;
  }
  return false;
}
//se o que recebeu foi rcv
bool test_communication() {
  bool result = false;

  nRF_receive();
  if (strcmp(RecvPayload,"rcv") == 0) {
    result = true;
  }
  
  return result;
}

bool wait(char* str) {
  bool result = false;

  nRF_receive();
  if (strcmp(RecvPayload,str) == 0) {
    result = true;
  }
  
  return result;
}
/*void serialEvent() {
  while (Serial.available() > 0 ) {
    char incomingByte = Serial.read();

    if (stringOverflow) {
      serialBuffer[dataBufferIndex++] = charOverflow;  // Place saved overflow byte into buffer
      serialBuffer[dataBufferIndex++] = incomingByte;  // saved next byte into next buffer
      stringOverflow = false;                          // turn overflow flag off
    } else if (dataBufferIndex > 31) {
      stringComplete = true;        // Send this buffer out to radio
      stringOverflow = true;        // trigger the overflow flag
      charOverflow = incomingByte;  // Saved the overflow byte for next loop
      dataBufferIndex = 0;          // reset the bufferindex
      break;
    }
    else if (incomingByte == '\n') {
      serialBuffer[dataBufferIndex] = 0;
      stringComplete = true;
    } else {
      serialBuffer[dataBufferIndex++] = incomingByte;
      serialBuffer[dataBufferIndex] = 0;
    }
  } // end while()
} */

void nRF_receive(void) {
  int len = 0;
  if ( radio.available() ) {
    bool done = false;
    while ( radio.available() ) {
      len = radio.getDynamicPayloadSize();
      radio.read(&RecvPayload, len);
      delay(5);
    }

    RecvPayload[len] = 0; // null terminate string

    //lcd.setCursor(0,0);
    //lcd.print("R:");
    Serial.print("R:");
    //lcd.setCursor(2,0);
    //lcd.print(RecvPayload);
    Serial.print(RecvPayload);
    Serial.println();
    //RecvPayload[0] = 0;  // Clear the buffers
  }

} // end nRF_receive()


unsigned long int uli_receive(void) 
{
  //int len = 0;
  //Serial.println("uli_receive");
  unsigned long int val;
  if ( radio.available() ) 
  {
    //bool done = false;
    while ( radio.available() ) 
    {
      radio.read(&val, sizeof(val));
      delay(5);
    }

    Serial.print("R(uli):");
    Serial.println(val);
    return val;

  }

} // end nRF_receive()

/*void serial_receive(void) {

  if (stringComplete) {
    strcat(SendPayload, serialBuffer);
    // swap TX & Rx addr for writing
    radio.openWritingPipe(pipes[1]);
    radio.openReadingPipe(0, pipes[0]);
    radio.stopListening();
    bool ok = radio.write(&SendPayload, strlen(SendPayload));

    //lcd.setCursor(0,1);
    //lcd.print("S:");
    Serial.print("S:");
    //lcd.setCursor(2,1);
    //lcd.print(SendPayload);
    Serial.print(SendPayload);
    Serial.println();
    stringComplete = false;

    // restore TX & Rx addr for reading
    radio.openWritingPipe(pipes[0]);
    radio.openReadingPipe(1, pipes[1]);
    radio.startListening();
    SendPayload[0] = 0;
    dataBufferIndex = 0;
  } // endif
} */

void serial_send(char* str) {

  strcat(SendPayload,str);
  // swap TX & Rx addr for writing
  radio.openWritingPipe(pipes[1]);
  radio.openReadingPipe(0, pipes[0]);
  radio.stopListening();
  bool ok = radio.write(&SendPayload, strlen(SendPayload));

  Serial.print("S:");

  Serial.print(SendPayload);
  Serial.println();
  stringComplete = false;

  // restore TX & Rx addr for reading
  radio.openWritingPipe(pipes[0]);
  radio.openReadingPipe(1, pipes[1]);
  radio.startListening();
  SendPayload[0] = 0;
  dataBufferIndex = 0;
} 
