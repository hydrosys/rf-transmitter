//Programa: Arduino GSM Shield SIM900 - Acesso internet
//Autor: Arduino e Cia

//#include "sms.h"
//#include <SoftwareSerial.h>

//see graphics: https://thingspeak.com/channels/160530
//id: hydrosys
//email: risa@cin.ufpe.br
//password: hydrosys1234
#include "inetGSM.h"
#include <cstdlib>

InetGSM inet;

unsigned int n1f = 0;
unsigned int n2f = 0;
unsigned int n1p = 0;
unsigned int n2p = 0;

char writeAPIKey[] = "9S879E2EF2M1MFR3";

boolean started = false;

void setup()
{
  Serial.begin(115200);
  Serial.println(F("Testando GSM Shield SIM900"));
  if (gsm.begin(9600))
  {
    Serial.println(F("\nstatus=READY"));
    started = true;
  }
  else Serial.println(F("\nstatus=IDLE"));
}

void loop()
{
  if (started) {
      //httpPOST();  
      //httpGET();
      
      n1f=random(0,1200);
      n2f=random(0,1200);
      n1p=random(0,800);
      n2p=random(0,800);

      thingspeak();
      Serial.println("delay 5s");
      delay(5000);
  }
}

void httpPOST()
{
  char temp_string[100] = "text=testando&user=galileo00";
  char msg[10];
  int numdata;
  if (inet.attachGPRS("claro.com.br", "claro", "claro"))
    Serial.println(F("status=Conectado..."));
  else Serial.println(F("status=Nao conectado !!"));
  delay(100);
  numdata = inet.httpPOST("hydrosys.net16.net", 80, "/post.php", temp_string, msg, 50);
  Serial.print("numdata: ");
  Serial.println(numdata);
  delay(5000);
}

void httpGET()
{
  char msg[1000];
  int numdata;
  if (inet.attachGPRS("claro.com.br", "claro", "claro"))
    Serial.println(F("status=Conectado..."));
  else Serial.println(F("status=Nao conectado !!"));
  delay(100);
  numdata = inet.httpGET("hydrosys.net16.net", 80, "/log.html", msg, 1000);
  Serial.print("numdata: ");
  Serial.println(numdata);
  Serial.print("msg: ");
  Serial.println(msg);
  delay(5000);
}

void thingspeak()
{    
  char itoaBuffer[8];
  char end_c[2];
  end_c[0]=0x1a;
  end_c[1]='\0';
  
  char sentMsg[100];
  char msg[1000];
  int numdata;
      
  char data1[50];
  String str1;
  str1 = String(n1f);
  str1.toCharArray(data1, 50);
  char data2[50];
  String str2;
  str2 = String(n2f);
  str2.toCharArray(data2, 50);
  char data3[50];
  String str3;
  str3 = String(n1p);
  str3.toCharArray(data3, 50);
  char data4[50];
  String str4;
  str4 = String(n2p);
  str4.toCharArray(data4, 50);
  
  if (inet.attachGPRS("claro.com.br", "claro", "claro"))
    Serial.println(F("status=Conectado..."));
  else Serial.println(F("status=Nao conectado !!"));
  delay(100);
  sprintf(sentMsg, "field1=%s&field2=%s&field3=%s&field4=%s", data1, data2, data3, data4);  
  if (inet.connectTCP("api.thingspeak.com", 80)) {
    Serial.println("connectTCP == true");
    Serial.println("connected to thingspeak");
    gsm.SimpleWrite("POST /update HTTP/1.1\r\n");
    gsm.SimpleWrite("Host: api.thingspeak.com\r\n");
    gsm.SimpleWrite("Connection: close\r\n");
    gsm.SimpleWrite("X-THINGSPEAKAPIKEY: ");
    gsm.SimpleWrite(writeAPIKey);
    gsm.SimpleWrite("\r\n");
    gsm.SimpleWrite("Content-Type: application/x-www-form-urlencoded\r\n");
    gsm.SimpleWrite("Content-Length: ");
    sprintf(sentMsg, "field1=%s&field2=%s&field3=%s&field4=%s", data1, data2, data3, data4);
    sprintf(itoaBuffer,"%d",strlen(sentMsg));
    gsm.SimpleWrite(itoaBuffer);
    gsm.SimpleWrite("\r\n\r\n");
    Serial.println(sentMsg);
    gsm.SimpleWrite(sentMsg);
    gsm.SimpleWrite("\r\n\r\n");
    gsm.SimpleWrite(end_c);
  }
  else
  {
    Serial.println("connectTCP == false");
  }
  
  switch(gsm.WaitResp(10000, 10, "SEND OK")) {
    case RX_TMOUT_ERR:
      Serial.println("timeout");
      break;
    case RX_FINISHED_STR_NOT_RECV:
      Serial.println("not received");
      break;
  }
  delay(50);
  
  numdata= gsm.read(msg, 1000);
  Serial.print("numdata: ");
  Serial.println(numdata);
  Serial.print("msg: ");
  Serial.println(msg);
}
