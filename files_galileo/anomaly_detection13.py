import csv
import statistics as st
import numpy as np
import math as mt

def openFile(file):
			
	#Open Train and Tests files		 
	X = []
	with open(file) as f1:
		lines = f1.read().splitlines()
    	for row in lines:
    		if(row != ""):
    			X.append(float(row))
	f1.close()

	return X

#confidence=0.99
def mean_confidence_interval(X_train, table):
	mean_S = st.mean(X_train)
	sd_S = st.stdev(X_train)
	n = len(X_train)
	
	if(n>100):
		t_value = table[100] #para maiores que isso o valor de t tende a ser igual
	else:	
		t_value = table[n-2] #n-1-1 pq a lista comeca de 0

	mean_min = mean_S - t_value*sd_S/np.sqrt(n)
	mean_max = mean_S + t_value*sd_S/np.sqrt(n)
	#print mean_min
	#print mean_max
	return mean_min,mean_max 	

def t_test(X_train, X_test, table):
	n_train = len(X_train)
	n_test = len(X_test)
	n = n_train

	numerator = np.absolute(st.mean(X_train) - st.mean(X_test))
	denominator = (np.sqrt(st.stdev(X_train)**2)/n_train + (st.stdev(X_test)**2)/n_test)

	T = float(numerator)/denominator

	if(n>100):
		t_value = table[100] #para maiores que isso o valor de t tende a ser igual
	else:	
		t_value = table[n-2] #n-1-1 pq a lista comeca de 0

	if(T > t_value): #Reject the null hypothesis that the two means are equal
		return True	
	else:
		return False

def analise(X_test,X_train, table):
	#Faz o teste pro sensor 1
	vazando = False

	if(X_test):
		min_value,max_value = mean_confidence_interval(X_train,table) #confidence interval
    	test_mean = np.mean(X_test)
	
    	if((test_mean >= min_value-3*st.stdev(X_train)) and (test_mean <= max_value + 3*st.stdev(X_train))):
        	vazando = False
        	
    	else:
        	vazando = True
        	
	return vazando

def normal(X_train, X_test):
	vazamento = 0
	normal = 0
	mu = st.mean(X_train)
	dev = st.stdev(X_train)

	for i in range(len(X_test)):
	#vazamento
		if( (X_test[i] > (mu + 3*dev)) or (X_test[i] < (mu - 3*dev)) ):
			vazamento += 1
		else: #normal
			normal += 1

	#caso o arquivo de testes esteja vazio
	if(vazamento == 0 and normal == 0):
		normal =1

	Probability = mt.ceil(float(vazamento)*100/(vazamento+normal))

#	print("Probability:")
#	print(Probability)

	if(Probability >= 90):
		return True #It is a leakage
	else:
		return False  #Is not a leakage	


def writeFile(vazando, X_train,X_test, file):
	
	if(vazando):

		file.write("1\n") #1 significa que ha vazamento
	else:
		file.write("0\n") #0 significa que NAO ha vazamento 

	if(X_train):
	#Escreve alguns valores de treino no arquivo txt
		for i in range(len(X_train)):
			if(i % 3 ==0):
				file.write('%d\n' % X_train[i])	

	file.write('%s\n' % 'ft') #indica fim do arquivo de treino			
				
	if(X_test):
    #Escreve alguns valores de fluxo no arquivo txt
		for i in range(len(X_test)):
			if(i % 3 == 0):
				file.write('%d\n' % X_test[i])

def z_test(X_train, X_test):

	mean_test = st.mean(X_test)
	mean_train = st.mean(X_train)
	std_test = st.stdev(X_test)
	std_train = st.stdev(X_train)
	vazando = False
	Z = np.absolute(mean_test-mean_train)/np.sqrt(std_test+std_train)
#	print Z
	if(Z < 2):
		vazando = False
	elif(Z> 2 and Z<2.5):
		vazando = False
	elif(Z>2.5 and Z< 10):
		vazando = False
	elif(Z>10):
		vazando = True

	return vazando			

def main():
	
	vazando11 = False 
	vazando21 = False
	vazando12 = False
	vazando22 =  False
	vazando13 = False
	vazando23 = False
	vazando1 = False
	vazando2 = False

	#Open T Table
	table = []
	with open('ttable.csv', 'rb') as f:
		reader = csv.reader(f,delimiter=',')
		for row in reader:
			table.append(float(row[0]))
	f.close()

	X_train1 = openFile("train_sensor1.txt")
	X_train2 = openFile("train_sensor2.txt")
	X_test1 = openFile("teste_sensor1.txt")
	X_test2 = openFile("teste_sensor2.txt")

	if(X_train1 and X_test1):
		#Method 1
		vazando11 = t_test(X_train1, X_test1,table)
		#Method 2
		#vazando12 = analise(X_test1,X_train1,table)
		vazando12 = z_test(X_test1,X_train1)
		#Method 3
		vazando13 = normal(X_train1,X_test1)

#		print "Sensor 1"

	#	print vazando11
	#	print vazando12
	#	print vazando13
	#	print ""

	if(X_train2 and X_test2):
		#Method 1	
		vazando21 = t_test(X_train2,X_test2,table)
		#Method 2
		#vazando22 = analise(X_test2,X_train2,table)	
		vazando22 = z_test(X_test2,X_train2)
		#Method 3
		vazando23 = normal(X_train2,X_test2)

	#	print "Sensor 2"
	#	print vazando21
	#	print vazando22
	#	print vazando23

		
	#votador
	if(X_test2): #se estiver se comunicando com arduino 2
		if((vazando21 and vazando22) or (vazando21 and vazando23) or (vazando22 and vazando23) or (vazando21 and vazando22 and vazando23)):
			if(X_test1): #se o sensor nao conseguir se comunicar, tem que ta com valor de vazamento Falso
				vazando1 = True
			vazando2 = True
			print "Vazando"
		else:
			vazando1 = False
			vazando2 = False
			print "OK"
	else: #se nao tiver, a decisao eh do arduino 1
		if((vazando11 and vazando12) or (vazando11 and vazando13) or (vazando12 and vazando13) or (vazando11 and vazando12 and vazando13)):
			vazando1 = True
			print vazando1

	#Escreve os resultados no arquivo
	file = open('results.txt', 'w')

	#calcular vazao estimada do vazamento
	if(X_test1 and X_test2):
		vazao = np.absolute(np.mean(X_test1) - np.mean(X_test2))
		if(vazando1==False and vazando2==False):
			vazao = 0
		file.write('%d\n' % vazao)
    #print vazao 
	elif(X_test1):
		vazao = np.absolute(np.mean(X_test1) - np.mean(X_train1))
		if(vazando1==False):
			vazao = 0
		file.write('%d\n' % vazao)     
	elif(X_test2):
		vazao =  np.absolute(np.mean(X_test2) - np.mean(X_train2))
		if(vazando2==False):
			vazao = 0
		file.write('%d\n' % vazao) 


	#Escreve para o sensor 1
	file.write('%s\n' % "s1")
	writeFile(vazando1,X_train1, X_test1,file)

	#Escreve para o sensor 2
	file.write('%s\n' % "s2")
	writeFile(vazando2,X_train2,X_test2,file)

	#indica que a analise acabou
	file = open('acabou.txt', 'w')
	file.close()

if __name__ == '__main__':
    main() 						
