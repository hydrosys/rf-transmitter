#!/bin/bash

date -s "2016-08-23 08:15:00"

ifconfig enp0s20f6 10.0.0.2 netmask 255.255.0.0 up
/sbin/route add default gw 10.0.0.1
echo "nameserver 8.8.8.8" >> /etc/resolv.conf
echo "nameserver 10.0.0.1" >> /etc/resolv.conf


connmanctl services                                                         # figure out your ethernet device eg. mine was: ethernet_984fee05a892_cable        
connmanctl config ethernet_984fee01efed_cable --nameservers 8.8.8.8 8.8.4.4          #these are Google's nameservers  

 
