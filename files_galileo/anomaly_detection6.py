import csv
import statistics as st
import numpy as np

#Variaveis

#confidence=0.99
def mean_confidence_interval(X_train1, table):
	mean_S = st.mean(X_train1)
	sd_S = st.stdev(X_train1)
	n = len(X_train1)
	
	if(n>100):
		t_value = table[100] #para maiores que isso o valor de t tende a ser igual
	else:	
		t_value = table[n-2] #n-1-1 pq a lista comeca de 0

	mean_min = mean_S - t_value*sd_S/np.sqrt(n)
	mean_max = mean_S + t_value*sd_S/np.sqrt(n)
	print mean_min
	print mean_max
	return mean_min,mean_max 

def openFile(file):
			
	#Open Train and Tests files		 
	X = []
	with open(file) as f1:
		lines = f1.read().splitlines()
    	for row in lines:
    		if(row != ""):
    			X.append(float(row))
	f1.close()

	return X

def analise(X_test,X_train, table):
	#Faz o teste pro sensor 1
	vazando = False

	if(X_test):
		min_value,max_value = mean_confidence_interval(X_train,table) #confidence interval
    	test_mean = np.mean(X_test)
	
    	if((test_mean >= min_value) and (test_mean <= max_value)):
        	vazando = False
        	print "OK"
    	else:
        	vazando = True
        	print "Vazando!"

	return vazando

def writeFile(vazando, X_test, file):
	
	if(vazando):
		file.write("1\n") #1 significa que ha vazamento
	else:
		file.write("0\n") #0 significa que NAO ha vazamento 

	if(X_test):
    #Escreve alguns valores de fluxo no arquivo txt
		for i in range(len(X_test)):
			if(i % 3 == 0):
				file.write('%d\n' % X_test[i])


def main():

	#Open T Table
	table = []
	with open('ttable.csv', 'rb') as f:
		reader = csv.reader(f,delimiter=',')
		for row in reader:
			table.append(float(row[0]))
	f.close()

	X_train1 = openFile("train_sensor1.txt")
	X_train2 = openFile("train_sensor2.txt")
	X_test1 = openFile("teste_sensor1.txt")
	X_test2 = openFile("teste_sensor2.txt")

	vazando1 = analise(X_test1,X_train1,table)
	vazando2 = analise(X_test2,X_train2,table)

	#Escreve os resultados no arquivo
	file = open('results.txt', 'w')

	#calcular vazao estimada do vazamento
#calcular vazao estimada do vazamento
	if(X_test1 and X_test2):
		vazao = np.absolute(np.mean(X_test1) - np.mean(X_test2))
		file.write('%.2f\n' % vazao)
    #print vazao 
	elif(X_test1):
		vazao = np.absolute(np.mean(X_test1) - np.mean(X_train1))
		file.write('%.2f\n' % vazao)     
	elif(X_test2):
		vazao =  np.absolute(np.mean(X_test2) - np.mean(X_train2))
		file.write('%.2f\n' % vazao) 
    	
	#Escreve para o sensor 1
	file.write('%s' % "s1")
	file.write("\n")
	writeFile(vazando1, X_test1,file)

	#Escreve para o sensor 2
	file.write('%s' % "s2")
	file.write("\n")
	writeFile(vazando2, X_test2,file)

	#indica que a analise acabou
	file = open('acabou.txt', 'w')
	file.close()

if __name__ == '__main__':
    main() 

