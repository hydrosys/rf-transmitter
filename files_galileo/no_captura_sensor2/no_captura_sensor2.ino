//Programa: Arduino GSM Shield SIM900 - Acesso internet
//Autor: Arduino e Cia
 
//#include "sms.h"
//#include <SoftwareSerial.h>
#include "inetGSM.h"
#include <cstdlib>
#include <fstream>
#include <iostream> 
 
InetGSM inet;
 
boolean started = false;
char smsbuffer[160] = "";
char smstemp[160] = "";
char n[20];
using namespace std;
ifstream nodeData;
byte valor;
 
//SMSGSM gsm1;
char position;
char phone_num[20]; // array for the phone number string
char sms_text[100]; // array for the SMS text string
 
void setup()
{
  Serial.begin(9600);
  //powerUpOrDown();
  Serial.println(F("Testando GSM Shield SIM900"));
  if (gsm.begin(9600))
  {
    Serial.println(F("\nstatus=READY"));
    started = true;
  }
  else Serial.println(F("\nstatus=IDLE deu ruim"));

  Serial.println(F("saiuu"));
  //started = true;
}
 
void loop()
{
  Serial.println("in loop");
  delay(1000);
  if (started) {
    Serial.println("after if started");
    //Aguarda novo SMS e envia para o servidor web
    if (gsm.readSMS(smsbuffer, 160, n, 20)) { //pede para usar getsms
    nodeData.open("results.txt");
    if (nodeData.is_open()) {
     while (!nodeData.eof()) {
        nodeData >> smstemp;
        Serial.println("smsbuffer: ");
        Serial.println(smsbuffer);
        strcat(smsbuffer,smstemp);
        strcpy(smstemp,"");
     }
    }
    nodeData.close();  
      Serial.println("after if readSMS");
      String str(smsbuffer);
      envia_GSM(smsbuffer);  
      Serial.println("before delay 10s");
      delay(10000);
      Serial.println("after delay 10s");
    }
    /*position = gsm.IsSMSPresent(SMS_UNREAD);
      if (position) {
      gsm.readSMS(position, phone_num, 20, sms_text, 160);
      Serial.println("DEBUG SMS phone number: ");
      Serial.println(phone_num);
      Serial.println("\r\n          SMS text: ");
      Serial.println(sms_text);
      String str(sms_text);
      envia_GSM(sms_text);
      delay(10000);
      }*/
      Serial.println("before delay 1s");
    delay(1000);
      Serial.println("after delay 1s");
  }
}
 
void powerUpOrDown()
{
  //Liga o GSM Shield
  Serial.print(F("Liga gsm..."));
  pinMode(6, OUTPUT);
  digitalWrite(6, LOW);
  delay(1000);
  digitalWrite(6, HIGH);
  delay(1000);
  Serial.println(F("OK!"));
  digitalWrite(6, LOW);
  delay(500);
}
 
void envia_GSM(String texto)
{
  Serial.println("envia_GSM");
  char temp_string[55];
  char msg[10];
  int numdata;
  if (inet.attachGPRS("claro.com.br", "claro", "claro"))
    Serial.println(F("status=Conectado..."));
  else Serial.println(F("status=Nao conectado !!"));
  delay(100);
  String valor = "MSG_Texto1=" + texto;
  valor.toCharArray(temp_string, 55);
  Serial.println("httpPOST");
  numdata = inet.httpPOST("hydrosys.net16.net", 80, "/add.php", temp_string, msg, 50);
  Serial.print("numdata: ");
  Serial.println(numdata);
  delay(5000);
  Serial.println("after delay 5s");
  //inet.dettachGPRS();
  Serial.println("after dettach");  

  }

