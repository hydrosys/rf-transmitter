/*#include <VirtualWire.h>
void setup()
{
    vw_setup(2400); // Bits per sec
}
void loop()
{
    const char *msg = "18";
    vw_send((uint8_t *)msg, strlen(msg));
    delay(2000);
}*/
#include <VirtualWire.h>

const int transmit_pin = 12;

void setup()
{
  Serial.begin(2400);
  // Initialise the IO and ISR
  vw_set_tx_pin(transmit_pin);
  vw_set_ptt_inverted(true); // Required for DR3100
  vw_setup(2400);   // Bits per sec
}


void loop()
{
  //char msg[5] = {'h','e','l','l','o'};
  const char *msg = "94";
  vw_send((uint8_t *)msg, strlen(msg));
  Serial.println(msg);

  //vw_send((uint8_t *)msg, 7);
  vw_wait_tx(); // Wait until the whole message is gone
  
  delay(1000);
  
}

