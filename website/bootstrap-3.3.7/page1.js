var active = "map-tab";
function initMap() {
  var locations = [
      ['Arduino Uno', -8.0552158, -34.9521292],
      ['Arduino Mega', -8.0551331, -34.9528499]];

  var mapDiv = document.getElementById('map');
  var map = new google.maps.Map(mapDiv, {
    center: {lat: -8.0551682, lng: -34.952471}, zoom: 18
  });
  var infowindow = new google.maps.InfoWindow();
  var marker = [];
  var i;
  for (i = 0; i < locations.length; i++) {
    marker[i] = new google.maps.Marker({
      position: new google.maps.LatLng(locations[i][1], locations[i][2]), 
      map: map,
      icon: {
        path: google.maps.SymbolPath.CIRCLE,
        fillColor: '#4D96F0',
        fillOpacity: 1.0,
        scale: 7,
        strokeColor: '#4D96F0',
        strokeWeight: 1
      }
    });

    google.maps.event.addListener(marker[i], 'click', (function(marker, i) {
      return function() {
        infowindow.setContent(locations[i][0]);
        infowindow.open(map, marker);
      }
    })(marker[i], i));
  };

  var route = [
    marker[0].position,
    marker[1].position
  ];

  var polyline = new google.maps.Polyline({
      path: route,
      strokeColor: "#35478C",
      strokeOpacity: 1.0,
      strokeWeight: 15
  });

  polyline.setMap(map);
}