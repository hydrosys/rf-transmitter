import numpy as np

X = []
with open('sensor1_semVazamento.txt') as f1:
    lines = f1.read().splitlines()
    for row in lines:
    	X.append(float(row))
    	
np.random.shuffle(X)
#print X

file = open('train_sensor1.txt', 'w')

for i in range(len(X)/2):
	file.write("%s " % X[i])
	file.write("\n")
file.close()

file = open('teste_sensor1.txt', 'w')
for i in range(len(X)/2+1,len(X)):
	file.write("%s " % X[i])
	file.write("\n")
file.close()