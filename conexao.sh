#!/bin/bash
ifconfig eth0 10.0.0.1
sudo iptables -A POSTROUTING -t nat -j MASQUERADE

sudo echo 1 > /proc/sys/net/ipv4/ip_forward
